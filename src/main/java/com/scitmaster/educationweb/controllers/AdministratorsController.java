package com.scitmaster.educationweb.controllers;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scitmaster.educationweb.dao.AdministratorsRepository;
import com.scitmaster.educationweb.vo.Administrators;

@Controller
public class AdministratorsController {

	@Autowired
	AdministratorsRepository repository;

	private static final Logger logger = LoggerFactory.getLogger(AdministratorsController.class);

	@RequestMapping(value = "/administrators", method = RequestMethod.GET)
	public String administrators(Administrators admin) {
		return "admin/administrators";
	}

	@RequestMapping(value = "/administratorsLogin", method = RequestMethod.POST)
	public String administratorsLogin(Administrators admin, HttpSession session, Model model) {
		logger.info("컨트롤단 administrator: 접속");
		
		if (admin != null) {
			logger.info("컨트롤단 administrator admin : " + admin);
			
			Administrators a = repository.selectLoginInfo(admin);
			logger.info("컨트롤단 administrator a : " + a);
			
			if (a == null) {
				String message = "Please, check your Id or Password";
				model.addAttribute("message", message);
			}
			
			if (a != null) {
				logger.info("컨트롤단 관리자 로그인 성공");
				logger.info("a.getAdministrator_id() : " + a.getAdministrator_id());
				logger.info("a.getAdministrator_nickname() : " + a.getAdministrator_nickname());
				
				session.setAttribute("administrator_id", a.getAdministrator_id());
				session.setAttribute("administrator_nickname", a.getAdministrator_nickname());
				
				return "redirect:/";
			}
		}

		return "admin/administrators";
	}

	// 로그아웃
	@RequestMapping(value = "/administratorsLogout", method = RequestMethod.GET)
	public String administratorsLogout(HttpSession session) {
		session.invalidate();
		logger.info("로그아웃 성공");
		
		return "redirect:/";
	}
	
}
