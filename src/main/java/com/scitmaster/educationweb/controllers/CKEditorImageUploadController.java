package com.scitmaster.educationweb.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class CKEditorImageUploadController {

	private static final Logger logger = LoggerFactory.getLogger(CKEditorImageUploadController.class);

	@RequestMapping(value = "/ckEditorImgUpload", method = RequestMethod.POST)
	public void ckEditorImgUpload(HttpServletRequest req, HttpServletResponse res, @RequestParam MultipartFile upload)
			throws Exception {

		logger.info("post CKEditor img upload");

		String resultMessage = "이미지를 업로드하였습니다.";
		boolean isImage = false;
		String filePath = "";

		// 인코딩
		res.setCharacterEncoding("utf-8");
		res.setContentType("text/html;charset=utf-8");

		// 랜덤 문자 생성
		UUID uid = UUID.randomUUID();

		OutputStream out = null;
		PrintWriter printWriter = null;

		try {
			printWriter = res.getWriter();
			String fileName = upload.getOriginalFilename(); // 파일 이름 가져오기

			byte[] bytes = upload.getBytes();
			HttpSession session = req.getSession();

			String rootPath = session.getServletContext().getRealPath("/"); // 웹서비스 root 경로
			String fileUrl = "resources/noticeUploadImage/" + uid + "_" + fileName; // 작성화면
			filePath = rootPath + fileUrl;
			
			File uploadFolder = new File(rootPath + "resources/noticeUploadImage/");
			if (!uploadFolder.isDirectory()) {
				uploadFolder.mkdirs();
			}
			
			out = new FileOutputStream(new File(filePath));
			out.write(bytes);
			out.flush(); // out에 저장된 데이터를 전송하고 초기화

			String callback = req.getParameter("CKEditorFuncNum");

			// 확장자명 저장
			String filenameExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
			
			// 확장자 검사 (이미지가 아니면 에러 메시지 저장 및 url 비우기)
			if (!filenameExtension.equals("jpg") && !filenameExtension.equals("jpeg")
					&& !filenameExtension.equals("png") && !filenameExtension.equals("gif")
					&& !filenameExtension.equals("bmp")) {
				
				isImage = false;

				fileUrl = "";
				resultMessage = "jpg, png, gif, bmp 파일만 업로드 가능합니다.";
			} else {
				isImage = true;
			}
			
			// 이미지 파일 업로드
			printWriter.println("<script>" + "window.parent.CKEDITOR.tools.callFunction(" + callback + ", '" + fileUrl
					+ "', '" + resultMessage + "');" + "</script>");
			printWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				
				if (printWriter != null) {
					printWriter.close();
				}
				
				if (!isImage) {
					File uploadFile = new File(filePath);
					
					if (uploadFile.exists()){
			            if (uploadFile.delete()) {
			            	logger.info("삭제 완료 : " + uploadFile.getName());
			            } else {
			            	logger.info("삭제 실패 : " + uploadFile.getName());
			            }
			        } else {
			        	logger.info("삭제할 파일이 존재하지 않습니다.");
			        }
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return;
	}
}
