﻿package com.scitmaster.educationweb.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scitmaster.educationweb.dao.NoticeRepository;
import com.scitmaster.educationweb.vo.Notice;

@Controller
public class HomeController {
	
	@Autowired
	NoticeRepository noticeRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "redirect:/index";
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		List<Notice> noticeList = noticeRepo.popUpNoticeList();
		model.addAttribute("noticeList", noticeList);

		return "index";
	}
	
	/*
	 * ======================================================================
	 * 이하는 편의용 임시 컨트롤러
	 */
	
	@RequestMapping(value = "/defaultStructure", method = RequestMethod.GET)
	public String header() {
		return "defaultStructure";
	}
	
	//관리자 자동 로그인
	@RequestMapping(value = "/RockMelonAutoLogin", method = RequestMethod.GET)
	public String utilTest(HttpSession session) {
		session.setAttribute("administrator_id", "jyoung4930@gmail.com");
		session.setAttribute("administrator_nickname", "RockMelon");
		
		return "redirect:/";
	}
	
	//참고
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error() {
		return "error";
	}
	
	//STT 
	@RequestMapping(value = "/sttTest", method = RequestMethod.GET)
	public String sttTest() {
		
		return "temp/sttTest";
	}
}
