﻿package com.scitmaster.educationweb.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scitmaster.educationweb.dao.MemberRepository;
import com.scitmaster.educationweb.vo.Member;

@Controller
public class MemberController {

	@Autowired
	MemberRepository repository;

	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, Model model) {
		String oldUrl = request.getHeader("referer");
		model.addAttribute("oldUrl", oldUrl);
		
		return "member/login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout() {
		return "member/logout";
	}
	
	/**
	 * 	email 여부 체크
	 */
	@RequestMapping(value = "/emailCheck", method = RequestMethod.GET)
	@ResponseBody
	public Member emailCheck(Model model, Member member, HttpSession session) {
		logger.info("컨트롤단 member: " + member.getMember_email());
		Member m = repository.selectOne(member);
//		logger.info("컨트롤단 m: " +m.getMember_email()); null 일때 오류나니가 주석처리
		
		return m;
	}
	
	/**
	 * 로그인 처리
	 */
	@RequestMapping(value = "/loginSession", method = RequestMethod.GET)
	@ResponseBody
	public String loginSession(Member member, HttpSession session) {
		logger.info("컨트롤단 loginSession member: " +member.getMember_email());
		Member m = repository.selectOne(member);
		
		// 아이디와 비밀번호를 잘못입력했을 경우 클라이언트단에서 사용할 메시지 저장
		// 입력을 잘 했을 경우 세션에 필요한 데이터 저장
//		logger.info("컨트롤단 loginSession m: " +m.getMember_email());
//		logger.info("컨트롤단 loginSession m: " +m.getMember_nickname());
//		session.setAttribute("loginEmail", m.getMember_email());
//		session.setAttribute("loginNickname", m.getMember_nickname());

		// 정보가 있다면 가입 된 회원이므로 세션 정보 뿌려주고 메인 페이지로 이동
		if (m != null) {
			// 정보가 없으면 비교 자체가 널포인트가 뜨기 때문에 막기 위해 널이 아닐때만 검색하도록 한다.
			if (m.getMember_email().equals(member.getMember_email())) {
				session.setAttribute("loginEmail", m.getMember_email());
				session.setAttribute("loginNickname", m.getMember_nickname());
				
				return "true";
			}
		}
		
		return "true";
	}

	/**
	 * 로그아웃 처리
	 * 
	 * @param session HttpSession객체
	 */
	@RequestMapping(value = "/googleLogout", method = RequestMethod.GET)
	public String googleLogout(HttpSession session) {
		session.invalidate();
		
		return "redirect:/";
	}

	/**
	 * 닉네임 화면으로 이동. 임시페이지
	 * 
	 * @param
	 */
	@RequestMapping(value = "/nickname", method = RequestMethod.GET)
	public String nickname(String member_email, Model model) {
		model.addAttribute("member_email", member_email);
		return "member/nickname";
	}

	/**
	 * 가입 처리
	 * 
	 * @param
	 */
	@RequestMapping(value = "/joinmember", method = RequestMethod.GET)
	public String joinmember(Member member, HttpSession session) {
//		logger.info(member.getMember_nickname());
		repository.joinMember(member);

		session.setAttribute("loginEmail", member.getMember_email());
		session.setAttribute("loginNickname", member.getMember_nickname());
		
		return "redirect:/";
	}

//	닉네임 중복 확인 화면 요청
	@RequestMapping(value = "/nicknameCheck", method = RequestMethod.GET)
	public String nicknameCheck() {
		return "member/nicknameCheck";
	}

//	닉네임 존재 여부 확인
	@RequestMapping(value = "/nicknameCheck", method = RequestMethod.POST)
	public String nicknameCheck(String member_nickname, Model model) {
		Member member = repository.selectNick(member_nickname);
		
		model.addAttribute("nickname", member_nickname);
		model.addAttribute("member", member);
		
		return "member/nicknameCheck";
	}

//	회원탈퇴
	@RequestMapping(value = "/deleteAccount", method = RequestMethod.GET)
	public String deleteAccount(HttpSession session, Member member) {
		member.setMember_email((String) session.getAttribute("loginEmail"));
		repository.deleteMember(member);
		session.invalidate();
		
		return "redirect:/";
	}
	
}
