package com.scitmaster.educationweb.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.scitmaster.educationweb.dao.AdministratorsRepository;
import com.scitmaster.educationweb.dao.NoticeRepository;
import com.scitmaster.educationweb.util.PageNavigator;
import com.scitmaster.educationweb.vo.Administrators;
import com.scitmaster.educationweb.vo.Notice;

@Controller
public class NoticeController {

	@Autowired
	NoticeRepository noticeRepo;

	@Autowired
	AdministratorsRepository adminRepo;

	private static final Logger logger = LoggerFactory.getLogger(NoticeController.class);

	// 공지사항 목록 페이지
	@RequestMapping(value = "/noticeList", method = RequestMethod.GET)
	public String noticeList(Model model,
			@RequestParam(value = "searchItem", defaultValue = "notice_name") String searchItem,
			@RequestParam(value = "searchWord", defaultValue = "") String searchWord,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage) {
		
		// searchItem 이 administrator_nickname 일때 id로 변환
		String nickname = null;
		String id = null;
		if(searchItem.equals("administrator_nickname")) {
			//nickname 가지러 가자
			searchItem = "administrator_id";
			if(searchWord.equals("")) {
				nickname = "";
			} else{
				Administrators admin = adminRepo.selectID(searchWord);
				if(admin !=null) {
					nickname = searchWord;
					id = admin.getAdministrator_id();
					searchWord = id;
				}
			}
		}
		
		int totalRecordCount = noticeRepo.totalNoticeCount(searchItem, searchWord);
		PageNavigator navi = new PageNavigator(currentPage, totalRecordCount);
		
		List<Notice> noticeList = noticeRepo.noticeList(searchItem, searchWord, navi.getStartRecord(),
				navi.getCountPerPage());
		
		for (Notice notice : noticeList) {
			String administrator_id = notice.getAdministrator_id();
			
			// 관리자 ID로 닉네임 불러오기
			String nickName = adminRepo.selectOne(administrator_id).getAdministrator_nickname();
			
			// 임의로 만든 변수 nickName에 값 set하기
			notice.setNickName(nickName);
		}
		if(searchItem.equals("administrator_id")) {
			searchWord = nickname;
			searchItem = "administrator_nickname";
		}
		int totalCount = noticeRepo.noticeCountAll(); // 공지글 전체 개수 (검색된 공지글 말고 전체)
		
		model.addAttribute("searchItem", searchItem);
		model.addAttribute("searchWord", searchWord);
		model.addAttribute("navi", navi);
		model.addAttribute("noticeList", noticeList);
		model.addAttribute("totalCount", totalCount);

		return "notice/noticeList";
	}

	// 공지사항 등록 페이지
	@RequestMapping(value = "/noticeRegist", method = RequestMethod.GET)
	public String noticeRegist() {
		return "notice/noticeRegist";
	}

	// 공지사항 등록하기
	@RequestMapping(value = "/noticeRegist", method = RequestMethod.POST)
	public String noticeRegist(Notice notice, HttpSession session) {
//		String administorId = (String) session.getAttribute("administorId");
//		notice.setAdministor_id(administorId);

		int result = noticeRepo.noticeRegist(notice);

		if (result != 1) {
			return "notice/noticeRegist";
		}

		return "redirect:/noticeList";
	}

	// 공지사항 자세히 보기 페이지
	@RequestMapping(value = "/noticeDetail", method = RequestMethod.GET)
	public String noticeDetail(int notice_num, Model model) {
		Notice notice = noticeRepo.noticeDetail(notice_num);
		
		// 관리자 ID로 닉네임 불러오기
		String administrator_id = notice.getAdministrator_id();
		String nickName = adminRepo.selectOne(administrator_id).getAdministrator_nickname();
		
		model.addAttribute("notice", notice);
		model.addAttribute("nickName", nickName);
		return "notice/noticeDetail";
	}

	// 공지사항 삭제하기
	@RequestMapping(value = "/noticeDelete", method = RequestMethod.GET)
	public String noticeDelete(int notice_num) {
		int result = noticeRepo.noticeDelte(notice_num);

		if (result != 1) {
			return "notice/noticeDetail";
		}

		return "redirect:/noticeList";
	}

	// 공지사항 수정 페이지
	@RequestMapping(value = "/noticeUpdate", method = RequestMethod.GET)
	public String noticeUpdate(int notice_num, Model model) {
		Notice notice = noticeRepo.noticeDetail(notice_num);
		
		// 관리자 ID로 닉네임 불러오기
		String administrator_id = notice.getAdministrator_id();
		String nickName = adminRepo.selectOne(administrator_id).getAdministrator_nickname();
		
		notice.setNickName(nickName);
		model.addAttribute("notice", notice);

		return "notice/noticeUpdate";
	}

	// 공지사항 수정하기
	@RequestMapping(value = "/noticeUpdate", method = RequestMethod.POST)
	public String noticeUpdate(Notice notice) {
		int result = noticeRepo.noticeUpdate(notice);

		if (result != 1) {
			return "notice/noticeUpdate";
		}

		return "redirect:/noticeList";
	}
}
