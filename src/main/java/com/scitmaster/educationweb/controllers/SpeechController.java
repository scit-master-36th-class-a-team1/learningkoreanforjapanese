package com.scitmaster.educationweb.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scitmaster.educationweb.util.SpeechToText;

@Controller
public class SpeechController {
	
	private static final Logger logger = LoggerFactory.getLogger(SpeechController.class);
	
	@RequestMapping(value = "/startSTT", method = RequestMethod.GET, produces = "application/text; charset=utf-8")
	@ResponseBody
	public String startSTT() {
		String outputText = "";
		
		try {
			outputText = SpeechToText.microphoneToText();
		
			logger.info("outputText : " + outputText);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return outputText;
	}
	
}
