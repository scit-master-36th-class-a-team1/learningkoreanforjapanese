package com.scitmaster.educationweb.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scitmaster.educationweb.dao.StudyCommentRepository;
import com.scitmaster.educationweb.vo.StudyComment;

@Controller
public class StudyCommentController {

	@Autowired
	StudyCommentRepository repository;

	// 원하는 게시글로 이동
//	@RequestMapping(value = "/commentTest", method = RequestMethod.GET)
//	public String commentTest(Model model) throws Exception {
	// 게시글 넘버는 일단 임의로 지정함
//		int study_num = 1;
//		Study study = repository.studyList(study_num);
//		model.addAttribute("study", study);
//		return "temp/commentTest";
//	}
	// 해당 게시글의 댓글 리스트 불러오기
	@RequestMapping(value = "/commentList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<StudyComment> commentList(Model model, int study_num, HttpSession session)
			throws Exception {
		
		ArrayList<StudyComment> comment = (ArrayList<StudyComment>) repository.commentList(study_num);
		String nickName = null;
		
		for (StudyComment studyComment : comment) {
			if (studyComment.getAdministrator_id() != null) {
				String administrator_id = studyComment.getAdministrator_id();
				
				// 관리자 닉네임 불러오기
				nickName = repository.adminNickname(administrator_id);
				studyComment.setNickName(nickName);
			}
			
			if (studyComment.getMember_email() != null) {
				String member_email = studyComment.getMember_email();
				
				// 회원 닉네임 불러오기
				nickName = repository.memNickname(member_email);
				studyComment.setNickName(nickName);
			}
		}
		
		model.addAttribute("comment", comment);
		
		return comment;
	}

	// 댓글 등록하기
	@RequestMapping(value = "/commentInsert", method = RequestMethod.GET)
	public @ResponseBody int commentInsert(StudyComment comment, StudyComment sc, HttpSession session)
			throws Exception {
		
		String administrator_id = (String) session.getAttribute("administrator_id");
		String member_email = (String) session.getAttribute("loginEmail");
		
		if (administrator_id != null) {
			sc.setAdministrator_id(administrator_id);
		} else if (member_email != null) {
			sc.setMember_email(member_email);
		}
		
		int result = repository.commentInsert(comment);
		
		return result;
	}

	// 댓글 수정하기
	@RequestMapping(value = "/commentUpdate", method = RequestMethod.GET)
	public @ResponseBody int commentUpdate(StudyComment comment) throws Exception {
		int result = repository.commentUpdate(comment);
		return result;
	}

	// 댓글 삭제하기
	@RequestMapping(value = "/commentDelete", method = RequestMethod.GET)
	public @ResponseBody int commentDelete(int study_comment_num) throws Exception {
		int result = repository.commentDelete(study_comment_num);
		return result;
	}

}
