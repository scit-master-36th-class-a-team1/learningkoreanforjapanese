﻿package com.scitmaster.educationweb.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scitmaster.educationweb.dao.StudyRepository;
import com.scitmaster.educationweb.util.DeleteFolder;
import com.scitmaster.educationweb.util.PageNavigator;
import com.scitmaster.educationweb.vo.Study;


@Controller
public class StudyController {

	@Autowired
	StudyRepository repository;
	
	private static final Logger logger = LoggerFactory.getLogger(StudyController.class);

	@RequestMapping(value = "/introduceKorean", method = RequestMethod.GET)
	public String introduceKorean() {
		return "study/introduceKorean";
	}
	
	//페이지 넘기면서, 어떤 타입인지 확인하고 데이타 넘겨주기.
	@RequestMapping(value = "/studyIndex", method = RequestMethod.GET)
	public String studyIndex(
			@RequestParam(value = "searchItem", defaultValue = "study_mainTitle") String searchItem,
			@RequestParam(value = "searchWord", defaultValue = "") String searchWord,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
			@RequestParam(value = "study_type", defaultValue = "") String study_type,
			Model model) {
		
		int totalRecordCount = repository.totalstudyCount(searchItem, searchWord, study_type); // search한 것의 전체 게시글 수
		PageNavigator navi = new PageNavigator(currentPage, totalRecordCount);
		List<Study> studyList = repository.selectType(searchItem, searchWord, navi.getStartRecord(), navi.getCountPerPage(), study_type);
		
		int totalCount = repository.studyCountAll(); // 학습글 전체 개수 (검색된 학습글 말고 전체)
		
		model.addAttribute("study_type", study_type);
		model.addAttribute("studyList", studyList);
		model.addAttribute("navi", navi);
		model.addAttribute("searchWord", searchWord);
		model.addAttribute("searchItem", searchItem);
		model.addAttribute("totalCount", totalCount);
		
		return "study/studyIndex";
	}
	
//	글 자세히 보기 => /study/studyDetail
	@RequestMapping(value = "/studyDetail", method = RequestMethod.GET)
	public String studyDetail(
			@RequestParam(value = "searchItem", defaultValue = "study_mainTitle") String searchItem,
			@RequestParam(value = "searchWord", defaultValue = "") String searchWord,
			@RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
			@RequestParam(value = "study_type", defaultValue = "level1") String study_type,
			int study_num, Model model) {
		DeleteFolder delete = new DeleteFolder();
		delete.delete();
		
		Study study = repository.studyDetail(study_num);
		// 업로드한 파일이 그림인지 아닌지.

		// 글 갯수.
		int totalRecordCount = repository.totalstudyCount(searchItem, searchWord, study_type);
		PageNavigator navi = new PageNavigator(currentPage, totalRecordCount);
		
		// 검색을 한 상태에서 글 자세히보기로 왔을 때를 처리하기 위해
		model.addAttribute("study", study);
		model.addAttribute("study_num", study_num);
		model.addAttribute("navi", navi);
		model.addAttribute("searchWord", searchWord);
		model.addAttribute("searchItem", searchItem);
		model.addAttribute("study_type", study_type);

		return "study/studyDetail";
	}

//	글 수정 => /study/studyUpdate.jsp 
	// 페이지 넘기기
	@RequestMapping(value = "/studyUpdate", method = RequestMethod.GET)
	public String studyUpdate(int study_num, Model model) {
		Study study = repository.studyDetail(study_num);
		model.addAttribute("study", study);

		return "study/studyUpdate";
	}

	/**
	 * 게시글 수정 처리 요청
	 */
	// DB 가서 수정
	@RequestMapping(value = "/studyUpdate", method = RequestMethod.POST)
	@ResponseBody
	public int studyUpdate(Study study) {
		if(study != null) {
			System.out.println(study);
			repository.studyUpdate(study);
		}
		
		return study.getStudy_num();
	}
	
//	글 삭제 => x
	@RequestMapping(value = "/studyDelete", method = RequestMethod.GET)
	public String studyDelete(int study_num, String study_type) {
		// DB에 저장된 글 삭제
		repository.delete(study_num);
		
		return "redirect:/studyIndex?study_type=" + study_type;
	}

	// 글쓰기 페이지
	@RequestMapping(value = "/studyRegist", method = RequestMethod.GET)
	public String studyRegist(String requestStudyType, Model model) {
		model.addAttribute("requestStudyType", requestStudyType);	
		
		return "study/studyRegist";
	}
		
//	글쓰기 처리
	@RequestMapping(value = "/studyRegist", method = RequestMethod.POST)
	@ResponseBody
	public int studyRegist(Study study,  HttpSession session, Model model) {
		logger.info("study : " + study);
		
		String administrator_id = (String) session.getAttribute("administrator_id");
		logger.info("administrator_id : " + administrator_id);
		
		if(administrator_id != null) {
			study.setAdministrator_id(administrator_id);
			int result = repository.studyRegist(study);
			
			logger.info("study : " + study.getStudy_num());
			
			List<Study> copyStudy = repository.studyNum(study);
			int study_num = copyStudy.get(0).getStudy_num();
			//글 등록이 끝이 나고 글 번호를 넘겨야 하는 코드를 here
			
//			session.setAttribute("study_num", study_num);
			return study_num;
		}
		
		return 0;
	}
	
}
