package com.scitmaster.educationweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scitmaster.educationweb.util.GoogleTextToSpeech;

@Controller
public class TextToSpeechController {
	
	@RequestMapping(value = "/speak", method = RequestMethod.GET)
	@ResponseBody
	public String speak(String text) {
		GoogleTextToSpeech g1 = new GoogleTextToSpeech();
		
		try {
			g1.test(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}
	
}
