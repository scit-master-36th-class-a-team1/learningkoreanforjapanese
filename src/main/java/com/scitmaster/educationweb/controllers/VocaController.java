package com.scitmaster.educationweb.controllers;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scitmaster.educationweb.dao.VocaRepository;
import com.scitmaster.educationweb.vo.Voca;


@Controller
public class VocaController {

	@Autowired
	VocaRepository repository;

	private static final Logger logger = LoggerFactory.getLogger(VocaController.class);
	
	//	보카등록  => /study/studyRegist.jsp 화면처리
	@RequestMapping(value = "/vocaRegist", method = RequestMethod.GET)
	public String vocaRegist(int study_num, String requestStudyType, Model model) {
		model.addAttribute("study_num", study_num);
		model.addAttribute("requestStudyType", requestStudyType);
		
		return "study/vocaRegist";
	}
	
	//리스트 불러오기
	@RequestMapping(value = "/vocaList", method = RequestMethod.GET)
	@ResponseBody 
	public ArrayList<Voca> vocaList(int study_num) {
		ArrayList<Voca> vList = new ArrayList<Voca>();
		
		try {
			vList = (ArrayList<Voca>) repository.vocaList(study_num);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return vList;
	}
	
	//보카 등록
	@RequestMapping(value = "/vocaWrite", method = RequestMethod.POST)
	@ResponseBody 
	public String vocaWrite(Voca voca) {
		logger.info("vocaWrite : "+ voca);
		repository.vocaWrite(voca);
		
		return "";
	}
	
	//보카 삭제
	@RequestMapping(value = "/vocaDelete", method = RequestMethod.POST)
	@ResponseBody
	public String reviewDelete(int voca_num) {
		int result = 0;
		result = repository.vocaDelete(voca_num);
		
		if (result == 0) {
			return "fail";
		}
		
		return "success";
	}
	
	// 보카 페이지 넘기기
	@RequestMapping(value = "/vocaUpdate", method = RequestMethod.GET)
	public String vocaUpdate(int study_num, String requestStudyType, Model model) {
		model.addAttribute("study_num", study_num);
		model.addAttribute("requestStudyType", requestStudyType);
		
		return "study/vocaUpdate";
	}

}
