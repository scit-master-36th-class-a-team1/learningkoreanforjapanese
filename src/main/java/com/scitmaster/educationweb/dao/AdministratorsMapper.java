package com.scitmaster.educationweb.dao;

import com.scitmaster.educationweb.vo.Administrators;

public interface AdministratorsMapper {
	
	// 관리자 아이디로 Administrators 검색하기
	Administrators selectOne(String inputAdmin);

	// 관리자 아이디로 Administrators 객체를 찾아 반환하기
	Administrators selectLoginInfo(Administrators admin);

	// 관리자 닉네임으로 Administrators 객체를 찾아 반환하기
	Administrators selectID(String searchWord);

}
