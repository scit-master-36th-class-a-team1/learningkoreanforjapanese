package com.scitmaster.educationweb.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scitmaster.educationweb.vo.Administrators;
import com.scitmaster.educationweb.vo.Member;

/**
 * 회원 관련 DAO
 */
@Repository
public class AdministratorsRepository {

	@Autowired
	SqlSession session;
	
	// 관리자 아이디로 Administrators 검색하기
	public Administrators selectOne(String inputAdmin) {
		AdministratorsMapper mapper = session.getMapper(AdministratorsMapper.class);
		Administrators admin = mapper.selectOne(inputAdmin);
		
		return admin;
	}

	// 입력받은 아이디와 비밀번호로 관리자 계정을 찾고, 계정이 존재한다면 Administrators 객체를 반환
	public Administrators selectLoginInfo(Administrators inputAdmin) {
		AdministratorsMapper mapper = session.getMapper(AdministratorsMapper.class);
		Administrators admin = mapper.selectLoginInfo(inputAdmin);
		
		return admin;
	}
	
	//입력받은 닉네임을 통해 아이디를 찾기
	public Administrators selectID(String searchWord) {
		AdministratorsMapper mapper = session.getMapper(AdministratorsMapper.class);
		Administrators admin = mapper.selectID(searchWord);
		return admin;
	}
	
}
