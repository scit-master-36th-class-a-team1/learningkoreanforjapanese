package com.scitmaster.educationweb.dao;

import java.util.List;

import com.scitmaster.educationweb.vo.Member;

public interface MemberMapper {

//	가입 된 회원 중 한명 찾기
	public Member selectOne(Member member);

//	멤버 리스트 가져오기
	public List<Member> memberList();

//	멤버중 닉네임 찾기
	public Member selectNick(String member_nickname);

//	멤버 회원 가입
	public int joinMember(Member member);

//	멤버 회원 탈퇴
	public int deleteMember(Member member);

}
