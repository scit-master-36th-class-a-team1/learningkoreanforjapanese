﻿package com.scitmaster.educationweb.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.scitmaster.educationweb.vo.Notice;

public interface NoticeMapper {
	
	// 팝업창용 공지글 불러오기
	List<Notice> popUpNoticeList();
	
	// 검색된 공지글 불러오기
	List<Notice> noticeList(Map<String, String> map, RowBounds rb);
	
	// 공지사항 등록하기
	int noticeRegist(Notice notice);
	
	// 공지사항 자세히 보기
	Notice noticeDetail(int notice_num);
	
	// 공지사항 삭제하기
	int noticeDelete(int notice_num);
	
	// 공지사항 수정하기
	int noticeUpdate(Notice notice);
	
	// 조회수 증가
	int incrementHitcount(int notice_num);
	
	// 검색된 공지글 개수 조회
	int noticeCount(Map<String, String> map);
	
	// 전체 공지글 개수 조회
	int noticeCountAll();
	
}
