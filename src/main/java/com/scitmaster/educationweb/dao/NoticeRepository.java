﻿package com.scitmaster.educationweb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scitmaster.educationweb.vo.Notice;

@Repository
public class NoticeRepository {
	
	@Autowired
	SqlSession session;
	
	// 팝업창용 공지글 불러오기
	public List<Notice> popUpNoticeList() {
		NoticeMapper mapper = session.getMapper(NoticeMapper.class);
		List<Notice> popUpNoticeList = mapper.popUpNoticeList();
		
		return popUpNoticeList;
	}
	
	// 검색된 공지글 불러오기
	public List<Notice> noticeList(String searchItem, String searchWord, int startRecord, int countPerPage) {
		RowBounds rb = new RowBounds(startRecord, countPerPage);
		NoticeMapper mapper = session.getMapper(NoticeMapper.class);
		
		Map<String, String> map = new HashMap<>();
		map.put("searchItem", searchItem);
		map.put("searchWord", searchWord);
		
		List<Notice> noticeList = mapper.noticeList(map, rb);	
		
		return noticeList;
	}
	
	// 공지사항 등록하기
	public int noticeRegist(Notice notice) {
		NoticeMapper dao = session.getMapper(NoticeMapper.class);
		int result = dao.noticeRegist(notice);
		
		return result;
	}
	
	// 공지사항 자세히 보기
	public Notice noticeDetail(int notice_num) {
		NoticeMapper dao = session.getMapper(NoticeMapper.class);
		
		// 조회수 증가
		Notice notice = dao.noticeDetail(notice_num);
		dao.incrementHitcount(notice_num);
		
		return notice;
	}
	
	// 공지사항 삭제하기
	public int noticeDelte(int notice_num) {
		NoticeMapper dao = session.getMapper(NoticeMapper.class);
		int result = dao.noticeDelete(notice_num);
		
		return result;
	}
	
	// 공지사항 수정하기
	public int noticeUpdate(Notice notice) {
		NoticeMapper dao = session.getMapper(NoticeMapper.class);
		int result = dao.noticeUpdate(notice);
		
		return result;
	}
	
	// 검색된 공지글 개수 조회
	public int totalNoticeCount(String searchItem, String searchWord) {
		NoticeMapper dao = session.getMapper(NoticeMapper.class);
		
		Map<String, String> map = new HashMap<>();
		map.put("searchItem", searchItem);
		map.put("searchWord", searchWord);
		
		int noticeCount = dao.noticeCount(map);
		
		return noticeCount;
	}
	
	// 전체 공지글 개수 조회
	public int noticeCountAll() {
		NoticeMapper mapper = session.getMapper(NoticeMapper.class);
		int noticeCount = mapper.noticeCountAll();
		
		return noticeCount;
	}
	
}
