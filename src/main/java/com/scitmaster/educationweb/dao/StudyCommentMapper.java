package com.scitmaster.educationweb.dao;

import java.util.List;

import com.scitmaster.educationweb.vo.Study;
import com.scitmaster.educationweb.vo.StudyComment;

public interface StudyCommentMapper {
	// 원하는 게시글로 이동
	public Study studyList(int study_num) throws Exception;
	// 해당 게시글의 댓글 리스트 불러오기
	public List<StudyComment> commentList(int study_num) throws Exception;
	// 댓글 등록하기
	public int commentInsert(StudyComment comment) throws Exception;
	// 댓글 수정하기
	public int commentUpdate(StudyComment comment) throws Exception;
	// 댓글 삭제하기
	public int commentDelete(int study_comment_num) throws Exception;
	// 관리자 닉네임 불러오기
	public String adminNickname(String administrator_id) throws Exception;
	// 회원 닉네임 불러오기
	public String memNickname(String member_email) throws Exception;
	
}
