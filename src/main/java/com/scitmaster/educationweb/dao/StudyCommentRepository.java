package com.scitmaster.educationweb.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scitmaster.educationweb.vo.Study;
import com.scitmaster.educationweb.vo.StudyComment;

@Repository
public class StudyCommentRepository {

	@Autowired
	SqlSession session;
	
	// 원하는 게시글로 이동
	public Study studyList(int study_num) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		Study study = mapper.studyList(study_num);
		return study;
	}
	// 해당 게시글의 댓글 리스트 불러오기
	public List<StudyComment> commentList(int study_num) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		List<StudyComment> comment = mapper.commentList(study_num);
		return comment;
	}
	// 댓글 등록하기
	public int commentInsert(StudyComment comment) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		int result = mapper.commentInsert(comment);
		return result;
	}
	// 댓글 수정하기
	public int commentUpdate(StudyComment comment) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		int result = mapper.commentUpdate(comment);
		return result;
	}
	// 댓글 삭제하기
	public int commentDelete(int study_comment_num) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		int result = mapper.commentDelete(study_comment_num);
		return result;
	}
	// 관리자 닉네임 불러오기
	public String adminNickname(String administrator_id) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		String nickName = mapper.adminNickname(administrator_id);
		return nickName;
	}
	// 회원 닉네임 불러오기
	public String memNickname(String member_email) throws Exception {
		StudyCommentMapper mapper = session.getMapper(StudyCommentMapper.class);
		String nickName = mapper.memNickname(member_email);
		return nickName;
	}
	
}
