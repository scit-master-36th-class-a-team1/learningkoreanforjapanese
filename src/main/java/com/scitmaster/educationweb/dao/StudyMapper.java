package com.scitmaster.educationweb.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.scitmaster.educationweb.vo.Study;

public interface StudyMapper {

	//study 개수 조회
	public int studyCount(Map<String, String> map);

	// 타입별 게시글 조회
	public List<Study> selectType(Map<String, String> map, RowBounds rb);

	public Study selectOne(int study_num);

	public int delete(int study_num);

	// study 글 등록
	public int insert(Study study);

	public int update(Study study);

	// 등록한 글 넘버 가지고 오기
	public List<Study> studyNum(Study study);
	
	// 전체 학습글 개수 조회
	public int studyCountAll();

	
}
