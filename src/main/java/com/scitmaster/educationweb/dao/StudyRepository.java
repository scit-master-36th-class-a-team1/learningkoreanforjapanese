package com.scitmaster.educationweb.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scitmaster.educationweb.vo.Study;


/**
 * 학습글 DAO
 */
@Repository
public class StudyRepository {

	@Autowired
	SqlSession session;

	public List<Study> selectType(String searchItem, String searchWord,
			int getStartRecord, int getCountPerPage, String study_type) {
		
		RowBounds rb = new RowBounds(getStartRecord, getCountPerPage);
		StudyMapper mapper = session.getMapper(StudyMapper.class);
		
		Map<String, String>map = new HashMap<String, String>();
		map.put("searchItem", searchItem);
		map.put("searchWord", searchWord);
		map.put("study_type", study_type);
		
		List<Study> studyList = mapper.selectType(map, rb);
		
		return studyList;
	}

	// 글 갯수 세기
	public int totalstudyCount(String searchItem, String searchWord, String study_type) {
		StudyMapper mapper = session.getMapper(StudyMapper.class);
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("searchItem", searchItem);
		map.put("searchWord", searchWord);
		map.put("study_type", study_type);
		
		int studyCount = mapper.studyCount(map);
		
		return studyCount;
	}

	// 글 정보 보기
	public Study studyDetail(int study_num) {
		StudyMapper mapper = session.getMapper(StudyMapper.class);
		Study study = mapper.selectOne(study_num);
		
		return study;
	}

	// 글 삭제
	public int delete(int study_num) {
		StudyMapper mapper = session.getMapper(StudyMapper.class);	
		int result = mapper.delete(study_num);
		
		return result;
	}

	// 글 등록
	public int studyRegist(Study study) {
		StudyMapper mapper = session.getMapper(StudyMapper.class);	
		int result = mapper.insert(study);
		
		return result;
	}
	
	// 글 수정
	public int studyUpdate(Study study) {
		StudyMapper mapper = session.getMapper(StudyMapper.class);
		int result = mapper.update(study);
		
		return result;
		
	}

	// 등록한 글 번호 가져오기
	public List<Study> studyNum(Study study) {
		StudyMapper mapper = session.getMapper(StudyMapper.class);
		return mapper.studyNum(study);
	}

	// 전체 학습글 개수 조회
	public int studyCountAll() {
		StudyMapper mapper = session.getMapper(StudyMapper.class);
		int studyCount = mapper.studyCountAll();
		
		return studyCount;
	}

}
