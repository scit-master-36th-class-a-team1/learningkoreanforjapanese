package com.scitmaster.educationweb.dao;

import java.util.ArrayList;

import com.scitmaster.educationweb.vo.Voca;

public interface VocaMapper {

	//보카 리스트
	public ArrayList<Voca> vocaList(int study_num) throws Exception;

	public int vocaEditNum(int study_num);

	public int copyData();

	public int vocaDelete(int voca_num);

	public int vocaWrite(Voca voca);

	
}
