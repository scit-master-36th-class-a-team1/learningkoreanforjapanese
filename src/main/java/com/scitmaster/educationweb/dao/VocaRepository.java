package com.scitmaster.educationweb.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scitmaster.educationweb.vo.Voca;


/**
 * voca DAO
 */
@Repository
public class VocaRepository {

	@Autowired
	SqlSession session;

	//보카 불러오기
	public ArrayList<Voca> vocaList(int study_num) {
		VocaMapper mapper = session.getMapper(VocaMapper.class);	
		ArrayList<Voca> vList = new ArrayList<Voca>();
		try {
			vList = (ArrayList<Voca>) mapper.vocaList(study_num);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vList;
	}

	public int vocaEditNum(int study_num) {
		VocaMapper mapper = session.getMapper(VocaMapper.class);
		int result = 0;
		try {
			result = mapper.vocaEditNum(study_num);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int copyData() {
		VocaMapper mapper = session.getMapper(VocaMapper.class);	
		int result = 0;
		try {
			result = mapper.copyData();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int vocaDelete(int voca_num) {
		VocaMapper mapper = session.getMapper(VocaMapper.class);
		int result = 0;
		try {
			result = mapper.vocaDelete(voca_num);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int vocaWrite(Voca voca) {
		VocaMapper mapper = session.getMapper(VocaMapper.class);
		int result = 0;
		try {
			result = mapper.vocaWrite(voca);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
}

