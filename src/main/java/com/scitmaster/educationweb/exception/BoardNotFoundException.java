package com.scitmaster.educationweb.exception;

import java.io.Serializable;

public class BoardNotFoundException extends Exception implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	// 생성자
	public BoardNotFoundException() {
		message = "Board not found.";
	}

	public String getMessage() {
		return message;
	}

}
