package com.scitmaster.educationweb.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 전역 기본 ExceptionHandler
 * 
 * @author User
 *
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);
	
	/**
	 * 404: Not Found Error
	 * 페이지가 존재하지 않음
	 * 
	 * @param request
	 * @param model
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	public String handlerNoHandlerFoundException(HttpServletRequest request, Model model, Exception exception) {
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		
		logger.error("Request: " + request.getRequestURL() + " raised " + sw.toString());

		String errorTitle = "存在しないページです!";
		String errorMessage = "要請したページを見つけませんでした。";
		String oldUrl = request.getHeader("referer");
		
		model.addAttribute("errorTitle", errorTitle);
		model.addAttribute("errorMessage", errorMessage);
		model.addAttribute("oldUrl", oldUrl);
		
		return "error";
	}
	
	/**
	 * 500: Internal Server Error
	 * 게시글이 존재하지 않음
	 * 
	 * @param request
	 * @param model
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(BoardNotFoundException.class)
	public String handlerBoardNotFoundException(HttpServletRequest request, Model model, Exception exception) {
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		
		logger.error("Request: " + request.getRequestURL() + " raised " + sw.toString());
		
		String errorTitle = "存在しない書き込みです!";
		String errorMessage = "要請した書き込みを見つけませんでした。";
		String oldUrl = request.getHeader("referer");
		
		model.addAttribute("errorTitle", errorTitle);
		model.addAttribute("errorMessage", errorMessage);
		model.addAttribute("oldUrl", oldUrl);
		
		return "error";
	}
	
	/**
	 * default error
	 * 
	 * @param request
	 * @param model
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public String handlerException(HttpServletRequest request, Model model, Exception exception) {
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		
		logger.error("Request: " + request.getRequestURL() + " raised " + sw.toString());
		
		String errorTitle = "分からない理由で問題が発生しました。";
		String errorMessage = "管理者に問い合わせてください。";
		String oldUrl = request.getHeader("referer");
		
		model.addAttribute("errorTitle", errorTitle);
		model.addAttribute("errorMessage", errorMessage);
		model.addAttribute("oldUrl", oldUrl);
		
		return "error";
	}
	
}
