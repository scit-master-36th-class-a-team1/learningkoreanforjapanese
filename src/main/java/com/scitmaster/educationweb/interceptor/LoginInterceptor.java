package com.scitmaster.educationweb.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(HandlerInterceptorAdapter.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		logger.info("Start PreHandle Check!");
		
		HttpSession session = request.getSession();
		String administrator_id = (String) session.getAttribute("administrator_id");
//		System.out.println(administrator_id);
		String member_email = (String) session.getAttribute("loginEmail");
//		System.out.println(member_email);
		
		if (administrator_id == null || member_email == null) {
//			System.out.println("flase 반환" + request.getContextPath() + "/login");
			response.sendRedirect(request.getContextPath() + "/login");
			return false;
		}
		
		return super.preHandle(request, response, handler);
	}

}
