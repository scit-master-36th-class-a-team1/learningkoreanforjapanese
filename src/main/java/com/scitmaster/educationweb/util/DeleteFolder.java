package com.scitmaster.educationweb.util;

import java.io.File;

public class DeleteFolder {
	
	//폴더와 하위파일들을 전부 삭제합니다. 폴더하위 폴더가 있을 경우 삭제되지 않습니다.
	public void delete() {
		String path = "C:/voca/mp3";
		File deleteFolder = new File(path);
		
		if(deleteFolder.exists()){
			File[] deleteFolderList = deleteFolder.listFiles();
			
			for (int j = 0; j < deleteFolderList.length; j++) {
				deleteFolderList[j].delete(); 
			}
			
			if(deleteFolderList.length == 0 && deleteFolder.isDirectory()){
				deleteFolder.delete();
			}
		}
		
		File file1 = new File(path); // 파일 생성
		if (!file1.exists()) { // 해당 경로에 디렉토리 없을 시
			file1.mkdirs(); // 디렉토리 형성
		}
	}
}
