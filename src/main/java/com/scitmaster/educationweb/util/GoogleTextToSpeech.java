package com.scitmaster.educationweb.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.google.cloud.texttospeech.v1.AudioConfig;
import com.google.cloud.texttospeech.v1.AudioEncoding;
import com.google.cloud.texttospeech.v1.SsmlVoiceGender;
import com.google.cloud.texttospeech.v1.SynthesisInput;
import com.google.cloud.texttospeech.v1.SynthesizeSpeechResponse;
import com.google.cloud.texttospeech.v1.TextToSpeechClient;
import com.google.cloud.texttospeech.v1.VoiceSelectionParams;
import com.google.protobuf.ByteString;

public class GoogleTextToSpeech {
	
	public void test(String text) throws Exception {
		
		//경로
		
		String path = "C:/voca/mp3";
		System.out.println(text);
		File file1 = new File(path); // 파일 생성
		if (!file1.exists()) { // 해당 경로에 디렉토리 없을 시
			file1.mkdirs(); // 디렉토리 형성
		}
		
		// 지정한 경로에 mp3생성 
		// 받아온 텍스트 내용으로 파일 생성
		File file2 = new File(path+"/"+ text+".mp3"); 
		
		// 고객을 인스턴스화하다		
		try (TextToSpeechClient textToSpeechClient = TextToSpeechClient.create()) {
			// 합성할 텍스트 입력 설정
			SynthesisInput input = SynthesisInput.newBuilder().setText(text).build();

			// 음성 요청을 작성하고 언어 코드("en-US")와 ssml 음성 성별("중립")을 선택한다.
			VoiceSelectionParams voice = VoiceSelectionParams.newBuilder().setLanguageCode("ko-KR").setSsmlGender(SsmlVoiceGender.NEUTRAL).build();

			// 반환할 오디오 파일 유형 선택
			AudioConfig audioConfig = AudioConfig.newBuilder().setAudioEncoding(AudioEncoding.MP3).build();

			// 선택한 음성 파라미터 및 오디오 파일 형식으로 텍스트 입력에 텍스트 음성 변환 요청 수행
			SynthesizeSpeechResponse response = textToSpeechClient.synthesizeSpeech(input, voice, audioConfig);

			// 응답에서 오디오 내용 가져오기
			ByteString audioContents = response.getAudioContent();

			// 출력 파일에 대한 응답을 작성한다
			try (OutputStream out = new FileOutputStream(file2)) {
				out.write(audioContents.toByteArray());
				System.out.println("mp3파일 정상적으로 생성됨");
			}
		}
	}
}
