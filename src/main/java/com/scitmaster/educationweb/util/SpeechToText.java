﻿package com.scitmaster.educationweb.util;

import java.util.ArrayList;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.TargetDataLine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
import com.google.cloud.speech.v1.RecognitionConfig;
import com.google.cloud.speech.v1.SpeechClient;
import com.google.cloud.speech.v1.SpeechRecognitionAlternative;
import com.google.cloud.speech.v1.StreamingRecognitionConfig;
import com.google.cloud.speech.v1.StreamingRecognitionResult;
import com.google.cloud.speech.v1.StreamingRecognizeRequest;
import com.google.cloud.speech.v1.StreamingRecognizeResponse;
import com.google.protobuf.ByteString;

public class SpeechToText {
	
	private static final Logger logger = LoggerFactory.getLogger(SpeechToText.class);
	private static String resultText = "";
	
	/**
	 * 마이크 입력을 받아 텍스트로 반환하는 메소드
	 * @return static String resultText
	 * @throws Exception
	 */
	public static String microphoneToText() throws Exception {

		ResponseObserver<StreamingRecognizeResponse> responseObserver = null;
		
		try (SpeechClient client = SpeechClient.create()) {
			// ResponseObserver 객체 생성
			responseObserver = new ResponseObserver<StreamingRecognizeResponse>() {
				private ArrayList<StreamingRecognizeResponse> responses = new ArrayList<>();
				
				public void onStart(StreamController controller) {
				}

				public void onResponse(StreamingRecognizeResponse response) {
					responses.add(response);
				}

				public void onComplete() {
					resultText = "";
					
					for (StreamingRecognizeResponse response : responses) {
						StreamingRecognitionResult result = response.getResultsList().get(0);
						SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
//						System.out.println(alternative.getConfidence());
						
						resultText = resultText + alternative.getTranscript();
					}
				}

				public void onError(Throwable t) {
					logger.error(t.getMessage());
				}
			};
			// ResponseObserver 객체 생성 끝

			ClientStream<StreamingRecognizeRequest> clientStream
				= client.streamingRecognizeCallable()
					.splitCall(responseObserver);
			
			// 레코딩 세팅
			RecognitionConfig recognitionConfig = RecognitionConfig.newBuilder()
					.setEncoding(RecognitionConfig.AudioEncoding.LINEAR16) // 오디오 인코딩
					.setLanguageCode("ko-KR") // 인식 언어
					.setSampleRateHertz(16000) // SampleRate
					.build();
			StreamingRecognitionConfig streamingRecognitionConfig = StreamingRecognitionConfig.newBuilder()
					.setConfig(recognitionConfig)
					.build();

			// StreamingRecognizeRequest 세팅
			StreamingRecognizeRequest request = StreamingRecognizeRequest.newBuilder()
					.setStreamingConfig(streamingRecognitionConfig).build();

			clientStream.send(request);
			
			// 오디오 포맷 세팅
			// SampleRate: 16000Hz, SampleSizeInBits: 16, Number of channels: 1
			// , Signed: true, bigEndian: false
			AudioFormat audioFormat = new AudioFormat(16000, 16, 1, true, false);
			
			// Set the system information to read from the microphone audio stream
			DataLine.Info targetInfo = new Info(TargetDataLine.class, audioFormat);

			// 마이크가 제공되는지 확인
			if (!AudioSystem.isLineSupported(targetInfo)) {
				logger.info("Microphone not supported");
				
				return "";
			}

			// Target data line captures the audio stream the microphone produces.
			TargetDataLine targetDataLine = (TargetDataLine) AudioSystem.getLine(targetInfo);
			targetDataLine.open(audioFormat);
			targetDataLine.start();
			logger.info("Start speaking");
			System.out.println("Start speaking");

			long startTime = System.currentTimeMillis();
			// Audio Input Stream
			AudioInputStream audio = new AudioInputStream(targetDataLine);

			// 마이크 소리를 읽고 StreamingRecognizeRequest에 정보를 담아서 보냄
			while (true) {
				long estimatedTime = System.currentTimeMillis() - startTime; // 읽어온 시간
				byte[] data = new byte[6400];
				audio.read(data); // 마이크 소리를 읽어옴
				
				if (estimatedTime > 2000) { // 목소리 인식을 시작하고 5초가 지나면 자동으로 멈춤
					logger.info("Stop speaking.");
					System.out.println("Stop speaking.");
					
					targetDataLine.stop();
					targetDataLine.close();
					
					break;
				}
				
				// 읽어온 정보를 보냄
				request = StreamingRecognizeRequest.newBuilder().setAudioContent(ByteString.copyFrom(data)).build();
				clientStream.send(request);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		// 읽어들인 정보를 전역변수 resultText에 저장함
		responseObserver.onComplete();
		
		return resultText;
	}
	
	
	
	
}