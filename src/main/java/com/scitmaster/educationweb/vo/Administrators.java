package com.scitmaster.educationweb.vo;

public class Administrators {

	private String administrator_id;
	private String administrator_password;
	private String administrator_nickname;

	public Administrators(String administrator_id, String administrator_password, String administrator_nickname) {
		super();
		this.administrator_id = administrator_id;
		this.administrator_password = administrator_password;
		this.administrator_nickname = administrator_nickname;
	}

	public Administrators() {
		super();
	}

	public String getAdministrator_id() {
		return administrator_id;
	}

	public void setAdministrator_id(String administrator_id) {
		this.administrator_id = administrator_id;
	}

	public String getAdministrator_password() {
		return administrator_password;
	}

	public void setAdministrator_password(String administrator_password) {
		this.administrator_password = administrator_password;
	}

	public String getAdministrator_nickname() {
		return administrator_nickname;
	}

	public void setAdministrator_nickname(String administrator_nickname) {
		this.administrator_nickname = administrator_nickname;
	}

	@Override
	public String toString() {
		return "Administrators [administrator_id=" + administrator_id + ", administrator_password="
				+ administrator_password + ", administrator_nickname=" + administrator_nickname + "]";
	}

}