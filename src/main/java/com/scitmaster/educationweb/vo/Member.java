package com.scitmaster.educationweb.vo;

public class Member {

	private String member_email;
	private String member_nickname;

	public Member(String member_email, String member_nickname) {
		super();
		this.member_email = member_email;
		this.member_nickname = member_nickname;
	}

	public Member() {
		super();
	}

	public String getMember_email() {
		return member_email;
	}

	public void setMember_email(String member_email) {
		this.member_email = member_email;
	}

	public String getMember_nickname() {
		return member_nickname;
	}

	public void setMember_nickname(String member_nickname) {
		this.member_nickname = member_nickname;
	}

	@Override
	public String toString() {
		return "Member [member_email=" + member_email + ", member_nickname=" + member_nickname + "]";
	}

}
