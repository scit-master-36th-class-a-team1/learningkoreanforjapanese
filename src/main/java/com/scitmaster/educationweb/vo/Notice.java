﻿package com.scitmaster.educationweb.vo;

public class Notice {

	private int notice_num;
	private String notice_name;
	private String notice_content;
	private int notice_hitcount;
	private String notice_date;
	private String administrator_id;
	private String nickName;

	public Notice() {
	}

	public Notice(int notice_num, String notice_name, String notice_content, int notice_hitcount, String notice_date,
			String administrator_id, String nickName) {
		super();
		this.notice_num = notice_num;
		this.notice_name = notice_name;
		this.notice_content = notice_content;
		this.notice_hitcount = notice_hitcount;
		this.notice_date = notice_date;
		this.administrator_id = administrator_id;
		this.nickName = nickName;
	}

	public int getNotice_num() {
		return notice_num;
	}

	public void setNotice_num(int notice_num) {
		this.notice_num = notice_num;
	}

	public String getNotice_name() {
		return notice_name;
	}

	public void setNotice_name(String notice_name) {
		this.notice_name = notice_name;
	}

	public String getNotice_content() {
		return notice_content;
	}

	public void setNotice_content(String notice_content) {
		this.notice_content = notice_content;
	}

	public int getNotice_hitcount() {
		return notice_hitcount;
	}

	public void setNotice_hitcount(int notice_hitcount) {
		this.notice_hitcount = notice_hitcount;
	}

	public String getNotice_date() {
		return notice_date;
	}

	public void setNotice_date(String notice_date) {
		this.notice_date = notice_date;
	}

	public String getAdministrator_id() {
		return administrator_id;
	}

	public void setAdministrator_id(String administrator_id) {
		this.administrator_id = administrator_id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Override
	public String toString() {
		return "Notice [notice_num=" + notice_num + ", notice_name=" + notice_name + ", notice_content="
				+ notice_content + ", notice_hitcount=" + notice_hitcount + ", notice_date=" + notice_date
				+ ", administrator_id=" + administrator_id + ", nickName=" + nickName + "]";
	}
	
}
