﻿package com.scitmaster.educationweb.vo;

public class Study {
	private int study_num;
	private String study_mainTitle;
	private String study_subTitle;
	private String study_content;
	private String study_date;
	private String study_type;
	private String study_chapter;
	private String administrator_id;
	public Study(int study_num, String study_mainTitle, String study_subTitle, String study_content, String study_date,
			String study_type, String study_chapter, String administrator_id) {
		super();
		this.study_num = study_num;
		this.study_mainTitle = study_mainTitle;
		this.study_subTitle = study_subTitle;
		this.study_content = study_content;
		this.study_date = study_date;
		this.study_type = study_type;
		this.study_chapter = study_chapter;
		this.administrator_id = administrator_id;
	}
	public Study() {
		super();
	}
	public int getStudy_num() {
		return study_num;
	}
	public void setStudy_num(int study_num) {
		this.study_num = study_num;
	}
	public String getStudy_mainTitle() {
		return study_mainTitle;
	}
	public void setStudy_mainTitle(String study_mainTitle) {
		this.study_mainTitle = study_mainTitle;
	}
	public String getStudy_subTitle() {
		return study_subTitle;
	}
	public void setStudy_subTitle(String study_subTitle) {
		this.study_subTitle = study_subTitle;
	}
	public String getStudy_content() {
		return study_content;
	}
	public void setStudy_content(String study_content) {
		this.study_content = study_content;
	}
	public String getStudy_date() {
		return study_date;
	}
	public void setStudy_date(String study_date) {
		this.study_date = study_date;
	}
	public String getStudy_type() {
		return study_type;
	}
	public void setStudy_type(String study_type) {
		this.study_type = study_type;
	}
	public String getStudy_chapter() {
		return study_chapter;
	}
	public void setStudy_chapter(String study_chapter) {
		this.study_chapter = study_chapter;
	}
	public String getAdministrator_id() {
		return administrator_id;
	}
	public void setAdministrator_id(String administrator_id) {
		this.administrator_id = administrator_id;
	}
	@Override
	public String toString() {
		return "Study [study_num=" + study_num + ", study_mainTitle=" + study_mainTitle + ", study_subTitle="
				+ study_subTitle + ", study_content=" + study_content + ", study_date=" + study_date + ", study_type="
				+ study_type + ", study_chapter=" + study_chapter + ", administrator_id=" + administrator_id + "]";
	}
	

	
}