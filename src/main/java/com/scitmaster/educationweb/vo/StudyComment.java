package com.scitmaster.educationweb.vo;

public class StudyComment {
	private int study_comment_num;
	private String study_comment;
	private String study_comment_date;
	private int study_num;
	private String member_email;
	private String administrator_id;
	private String nickName = null;
	
	public StudyComment(int study_comment_num, String study_comment, String study_comment_date, int study_num,
			String member_email, String administrator_id, String nickName) {
		super();
		this.study_comment_num = study_comment_num;
		this.study_comment = study_comment;
		this.study_comment_date = study_comment_date;
		this.study_num = study_num;
		this.member_email = member_email;
		this.administrator_id = administrator_id;
		this.nickName = nickName;
	}

	public StudyComment() {
		// TODO Auto-generated constructor stub
	}

	public int getStudy_comment_num() {
		return study_comment_num;
	}

	public void setStudy_comment_num(int study_comment_num) {
		this.study_comment_num = study_comment_num;
	}

	public String getStudy_comment() {
		return study_comment;
	}

	public void setStudy_comment(String study_comment) {
		this.study_comment = study_comment;
	}

	public String getStudy_comment_date() {
		return study_comment_date;
	}

	public void setStudy_comment_date(String study_comment_date) {
		this.study_comment_date = study_comment_date;
	}

	public int getStudy_num() {
		return study_num;
	}

	public void setStudy_num(int study_num) {
		this.study_num = study_num;
	}

	public String getMember_email() {
		return member_email;
	}

	public void setMember_email(String member_email) {
		this.member_email = member_email;
	}

	public String getAdministrator_id() {
		return administrator_id;
	}

	public void setAdministrator_id(String administrator_id) {
		this.administrator_id = administrator_id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Override
	public String toString() {
		return "StudyComment [study_comment_num=" + study_comment_num + ", study_comment=" + study_comment
				+ ", study_comment_date=" + study_comment_date + ", study_num=" + study_num + ", member_email="
				+ member_email + ", administrator_id=" + administrator_id + ", nickName=" + nickName + "]";
	}
	
}
