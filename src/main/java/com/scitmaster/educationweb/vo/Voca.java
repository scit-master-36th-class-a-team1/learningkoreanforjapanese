package com.scitmaster.educationweb.vo;

public class Voca {
	private int voca_num; 
	private String voca_ko;
	private String voca_jp;
	private String voca_yomi;
	private int study_num;
	public Voca(int voca_num, String voca_ko, String voca_jp, String voca_yomi, int study_num) {
		super();
		this.voca_num = voca_num;
		this.voca_ko = voca_ko;
		this.voca_jp = voca_jp;
		this.voca_yomi = voca_yomi;
		this.study_num = study_num;
	}
	public Voca() {
		super();
	}
	public int getVoca_num() {
		return voca_num;
	}
	public void setVoca_num(int voca_num) {
		this.voca_num = voca_num;
	}
	public String getVoca_ko() {
		return voca_ko;
	}
	public void setVoca_ko(String voca_ko) {
		this.voca_ko = voca_ko;
	}
	public String getVoca_jp() {
		return voca_jp;
	}
	public void setVoca_jp(String voca_jp) {
		this.voca_jp = voca_jp;
	}
	public String getVoca_yomi() {
		return voca_yomi;
	}
	public void setVoca_yomi(String voca_yomi) {
		this.voca_yomi = voca_yomi;
	}
	public int getStudy_num() {
		return study_num;
	}
	public void setStudy_num(int study_num) {
		this.study_num = study_num;
	}
	@Override
	public String toString() {
		return "Voca [voca_num=" + voca_num + ", voca_ko=" + voca_ko + ", voca_jp=" + voca_jp + ", voca_yomi="
				+ voca_yomi + ", study_num=" + study_num + "]";
	}
	
	
}
