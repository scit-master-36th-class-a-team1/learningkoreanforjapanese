﻿DROP TABLE Administrators CASCADE CONSTRAINTS;
DROP TABLE Note CASCADE CONSTRAINTS;
DROP TABLE Voca CASCADE CONSTRAINTS;
DROP TABLE Study CASCADE CONSTRAINTS;
DROP TABLE Study_Comment CASCADE CONSTRAINTS;
DROP TABLE Member CASCADE CONSTRAINTS;
DROP TABLE Notice CASCADE CONSTRAINTS;
DROP SEQUENCE STUDY_SEQ;
DROP SEQUENCE STUDY_COMMENT_SEQ;
DROP SEQUENCE NOTE_SEQ;
DROP SEQUENCE VOCA_SEQ;
DROP SEQUENCE NOTICE_SEQ;

/* Create Tables */
CREATE TABLE Administrators
(
	administrator_id varchar2(20),
	administrator_password varchar2(20) NOT NULL,
	administrator_nickname varchar(20) UNIQUE NOT NULL,
	PRIMARY KEY (administrator_id)
);

CREATE TABLE Member
(
	member_email varchar2(100),
	member_nickname varchar2(20) NOT NULL UNIQUE,
	PRIMARY KEY (member_email)
);

CREATE TABLE Note
(
	note_num number,
	voca_num number NOT NULL UNIQUE,
	member_email varchar2(100) NOT NULL UNIQUE,
	PRIMARY KEY (note_num)
);

CREATE TABLE Study
(
	study_num number,
	study_mainTitle varchar2(1000) NOT NULL,
	study_subTitle varchar2(2000) NOT NULL,
	study_content varchar2(4000) NOT NULL,
	study_date date DEFAULT sysdate,
	study_type varchar2(100) NOT NULL,
	study_chapter varchar2(100) NOT NULL,
	administrator_id varchar2(20) NOT NULL,
	PRIMARY KEY (study_num)
);

CREATE TABLE Study_Comment
(
 study_comment_num number,
 study_comment varchar2(1000) NOT NULL,
 study_comment_date date DEFAULT sysdate,
 study_num number NOT NULL,
 member_email varchar2(100),
 administrator_id varchar2(20),
 PRIMARY KEY (study_comment_num)
);

CREATE TABLE Voca
(
	voca_num number,
	voca_ko varchar2(2000),
	voca_jp varchar2(2000),
	voca_yomi varchar2(2000),
	study_num number NOT NULL,
	PRIMARY KEY (voca_num)
);


CREATE TABLE Notice
(
	notice_num number,
	notice_name varchar2(200) NOT NULL,
	notice_content varchar2(4000) NOT NULL,
	notice_hitcount number default 0,
	notice_date date default sysdate,
	administrator_id varchar2(20),
	PRIMARY KEY (notice_num)
);

/* Create Foreign Keys */
ALTER TABLE Study
	ADD FOREIGN KEY (administrator_id)
	REFERENCES Administrators (administrator_id) 
;

ALTER TABLE Note
	ADD FOREIGN KEY (member_email)
	REFERENCES Member (member_email)
	on delete cascade
;

ALTER TABLE Study_Comment
	ADD FOREIGN KEY (member_email)
	REFERENCES Member (member_email)
	on delete cascade
;

ALTER TABLE Study_Comment
	ADD FOREIGN KEY (administrator_id)
	REFERENCES Administrators (administrator_id)
;

ALTER TABLE Study_Comment
	ADD FOREIGN KEY (study_num)
	REFERENCES Study (study_num)
	on delete cascade
;

ALTER TABLE Voca
	ADD FOREIGN KEY (study_num) 
	REFERENCES Study (study_num) 
	on delete cascade
;

ALTER TABLE Note
	ADD FOREIGN KEY (voca_num)
	REFERENCES Voca (voca_num)
	on delete cascade
;

ALTER TABLE Notice
    ADD FOREIGN KEY (administrator_id)
    REFERENCES Administrators (administrator_id)
;

/* Sequence */
CREATE SEQUENCE STUDY_SEQ;
CREATE SEQUENCE STUDY_COMMENT_SEQ;
CREATE SEQUENCE NOTE_SEQ;
CREATE SEQUENCE VOCA_SEQ;
CREATE SEQUENCE NOTICE_SEQ;

INSERT INTO Administrators values('cjw3402@naver.com',1,'TENA');
INSERT INTO Administrators values('jyoung4930@gmail.com',1,'RockMelon');
INSERT INTO Administrators values('ryuhw55@naver.com',1,'ひわ');
INSERT INTO Administrators values('dlwntjd13@naver.com',1,'ほたる');

INSERT INTO Notice
    (
        notice_num,
        notice_name,
        notice_content,
        administrator_id
    )
values
    (
        NOTICE_SEQ.NEXTVAL,
        '초급 과정 업데이트 안내',
        '한국동화 흥부와 놀부 3부가 업데이트 되었습니다. 많은 이용바랍니다.',
        'dlwntjd13@naver.com'
    );

INSERT INTO Study
    (
        study_num,
        study_mainTitle, 
        study_subTitle,
        study_content,
        study_type,
        study_chapter,
        administrator_id
    )
values
    (
        STUDY_SEQ.NEXTVAL,
        '처음 배우는 한국어',
        '한국 동화 흥부와 놀부에 대해 공부하는 시간을 가져봅시다',
        '한국동화 흥부와 놀부 3부가 드디어!',
        'level1',
        '동화_유아레벨',
        'jyoung4930@gmail.com'
    );

INSERT INTO Study
    (
        study_num,
        study_mainTitle, 
        study_subTitle,
        study_content,
        study_type,
        study_chapter,
        administrator_id
    )
values
    (
        STUDY_SEQ.NEXTVAL,
        '두번째 배우는 한국어',
        '한국 동화 흥부와 놀부에 대해 두번째로 공부하는 시간을 가져봅시다',
        '놀부가 박씨를 그만!.',
        'level1',
        '동화_유아레벨',
        'jyoung4930@gmail.com'
    );

INSERT INTO Study_Comment
    (
        study_comment_num,
        study_comment,
        study_num,
        ADMINISTRATOR_ID
    )
values
    (
        STUDY_COMMENT_SEQ.NEXTVAL,
        '홈페이지 구성이 너무 좋아요!',
        1,
        'dlwntjd13@naver.com'
    ); 

commit;    