<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="jp">
<head>
	<title>Administrator Login | K.Du</title>
	<meta charset="UTF-8">
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script type="text/javascript">
		$('#message').html(' ');
	</script>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--================================-->	
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/bootstrap/css/bootstrap.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--================================-->	
	<!-- 	<link rel="stylesheet" type="text/css" href="resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/animate/animate.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/css-hamburgers/hamburgers.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/animsition/css/animsition.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/select2/select2.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/daterangepicker/daterangepicker.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/css/util.css">
	<link rel="stylesheet" type="text/css" href="resources/css/main.css">
	<!--================================-->	
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-90 p-b-30">
				<form class="login100-form validate-form" action="administratorsLogin" method="post">
					<span class="login100-form-title p-b-40">
						Administrator Login
					</span>

					<div class="text-center p-t-55 p-b-30">
						<span class="txt1">
							Login with email
						</span>
						<br>
						<span id = message class="txt1">
							${message }
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-16" data-validate="Please enter email: ex@abc.xyz">
						<input class="input100" type="text" name="administrator_id" placeholder="Email">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-20" data-validate = "Please enter password">
						<span class="btn-show-pass">
							<i class="fa fa fa-eye"></i>
						</span>
						<input class="input100" type="password" name="administrator_password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<a href="index" class="txt2 p-b-10">
						← Back
					</a>
				</form>
			</div>
		</div>
	</div>
	
	<!--================================-->	
	<script src="resources/vendor/animsition/js/animsition.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/bootstrap/js/popper.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/select2/select2.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/daterangepicker/moment.min.js"></script>
	<script src="resources/vendor/daterangepicker/daterangepicker.js"></script>
	<!--================================-->	
	<script src="resources/vendor/countdowntime/countdowntime.js"></script>
	<!--================================-->	
	<script src="resources/js/jquery-ui.js"></script>
	<!--================================-->	
	<script src="resources/js/main.js"></script>
	<!--================================-->	
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/superfish.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
</body>
</html>