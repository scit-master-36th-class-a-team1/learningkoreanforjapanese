<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
</head>
<body>
	<header id="header">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
<!-- 		  				<ul> -->
<!-- 							<li><a href="#"><i class="fa fa-facebook"></i></a></li> -->
<!-- 							<li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
<!-- 							<li><a href="#"><i class="fa fa-dribbble"></i></a></li> -->
<!-- 							<li><a href="#"><i class="fa fa-behance"></i></a></li> -->
<!-- 		  				</ul> -->
					</div>
					
					<div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
<!--                         <a href ="sttTest">sttTest</a> -->
                        
						<!-- 구글 로그인 -->
                        <c:if test="${sessionScope.administrator_id == null}">
	                        <c:if test="${sessionScope.loginEmail == null}">
<!-- 	                           	<a href="RockMelonAutoLogin"> -->
<!--                     			    <span class="text">RockMelon Auto Login</span> -->
<!--                    			    </a> -->
	                            <a href="login">
	                                <span class="lnr lnr-phone-handset"></span>
	                                <span class="logbnt">ログイン</span>
	                            </a>
	                        </c:if>
							
							<c:if test="${sessionScope.loginEmail != null}">
								<!-- 지우지 마세요!! -->
								<div class="g-signin2" data-longtitle="true" onclick="onSignIn" hidden="hidden"></div>
								
								<a class="aTagCusor" onclick="signOut();">
									<span class="lnr lnr-phone-handset"></span>
									<span class="logbnt">ログアウト</span>
								</a>
								<a class="aTagCusor" onclick="deleteAccount();">
									<span class="lnr lnr-phone-handset"></span>
									<span class="logbnt">脱退</span>
								</a>
							</c:if>
                        </c:if>
                        
                        <c:if test="${sessionScope.administrator_id != null}">
                        	<a href="administratorsLogout">
	                        	<span class="lnr lnr-phone-handset"></span>
		                  		<span class="logbnt">관리자 ${administrator_nickname}님 Log Out</span>
	                  		</a>
                        </c:if>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="index">
						<img src="resources/img/logo.png" alt="" title="" />
					</a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li><a href="index">Home</a></li>
						<li><a href="noticeList">Notice</a></li>
						<li class="menu-has-children">
							<a href="introduceKorean">Education</a>
							<ul>
								<li><a href="introduceKorean">ハングルとは？</a></li>
								<li><a href="studyIndex?study_type=level1">LEVEL 1</a></li>
								<li><a href="studyIndex?study_type=level2">LEVEL 2</a></li>
								<li><a href="studyIndex?study_type=level3">LEVEL 3</a></li>
							</ul>
						</li>
						<li class="menu-has-children">
							<a href="" onclick="alert('まだ準備中の機能です。\n楽しんで待ってください。');">My Page</a>
<!-- 							<ul> -->
<!-- 								<li><a href="profile">Profile</a></li> -->
<!-- 								<li><a href="event-details.html">Event Details</a></li> -->
<!-- 								<li><a href="elements">Elements</a></li> -->
<!-- 							</ul> -->
						</li>
					</ul>
				</nav>
				<!-- #nav-menu-container -->
			</div>
		</div>
	</header>
	
	<footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h4>FAST MIGRATION</h4>
						<ul>
							<li><a href="index">HOME</a></li>
							<li><a href="noticeList">NOTICE</a></li>
							<li><a class="aTagCusor" onclick="alert('まだ準備中の機能です。\n楽しんで待ってください。');">MY PAGE</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h4>EDUCATION</h4>
						<ul>
							<li><a href="introduceKorean">ハングルとは？</a></li>
							<li><a href="studyIndex?study_type=level1">LEVEL 1</a></li>
							<li><a href="studyIndex?study_type=level2">LEVEL 2</a></li>
							<li><a href="studyIndex?study_type=level3">LEVEL 3</a></li>
						</ul>
					</div>
				</div>
				
				<div class="col-lg-2 col-md-6 col-sm-6"></div>
				<div class="col-lg-2 col-md-6 col-sm-6"></div>
				
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<img src="resources/img/logo2.png" alt="" title="" />
					</div>
				</div>
			</div>
			
			<div class="footer-bottom row align-items-center justify-content-between">
				<p class="footer-text m-0 col-lg-6 col-md-12">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>
					All rights reserved | This template is made with
					<i class="fa fa-heart-o" aria-hidden="true"></i>
					 by 
					<a href="https://colorlib.com" target="_blank">Colorlib</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
				
				<div class="col-lg-6 col-sm-12 footer-social">
<!-- 					<a href="#"><i class="fa fa-facebook"></i></a> -->
<!-- 					<a href="#"><i class="fa fa-twitter"></i></a> -->
<!-- 					<a href="#"><i class="fa fa-dribbble"></i></a> -->
<!-- 					<a href="#"><i class="fa fa-behance"></i></a> -->
				</div>
			</div>
		</div>
	</footer>
</body>
</html>