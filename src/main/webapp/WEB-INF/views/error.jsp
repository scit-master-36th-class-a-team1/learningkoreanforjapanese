<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="jp">
<head>
	<title>エラー発生 | K.Du</title>
	<meta charset="UTF-8">
	
	<!-- google -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===================================-->
	<link href="https://fonts.googleapis.com/css?family=Staatliches" rel="stylesheet">
	<!--===================================-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/bootstrap/css/bootstrap.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===================================-->
<!-- 	<link rel="stylesheet" type="text/css" href="resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/animate/animate.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/css-hamburgers/hamburgers.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/animsition/css/animsition.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/select2/select2.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/daterangepicker/daterangepicker.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/css/util.css">
	<link rel="stylesheet" type="text/css" href="resources/css/main.css">
	<!--===================================-->
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
</head>
<body>
	<div>
		<div class="container-error">
			<div>
				<span class="error-title p-b-40">
					<span class="error-simbol fa fa-exclamation"></span>&nbsp;&nbsp;ERROR
				</span>
				
				<div class="error-subtitle">
					${errorTitle }
				</div>
				
				<div class="mt-20 error-contents">
					${errorMessage }
				</div>
				
				<div class="mt-20">
					<a class="errorlink" href="${oldUrl }">
						<span class="fa fa-reply"></span>&nbsp;&nbsp;Back
					</a>
					<a class="pl-10 errorlink" href="index">
						<span class="fa fa-home"></span>&nbsp;&nbsp;Home
					</a>
				</div>
			</div>
		</div>
	</div>
	
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<!--===================================-->
	<script src="resources/vendor/animsition/js/animsition.min.js"></script>
	<!--===================================-->
	<script src="resources/vendor/bootstrap/js/popper.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===================================-->
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<!--===================================-->
	<script src="resources/vendor/select2/select2.min.js"></script>
	<!--===================================-->
	<script src="resources/vendor/daterangepicker/moment.min.js"></script>
	<script src="resources/vendor/daterangepicker/daterangepicker.js"></script>
	<!--===================================-->
	<script src="resources/vendor/countdowntime/countdowntime.js"></script>
	<!--===================================-->
	<script src="resources/js/main.js"></script>
</body>
</html>