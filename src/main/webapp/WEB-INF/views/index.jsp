﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>		
<!DOCTYPE html>
<html lang="jp">
<head>
	<!-- Site Title -->
	<title>K.Du</title>
	
	<!-- POPUP setCookie -->
	<script>
		function setCookie(name, value, expiredays) {
			var todayDate = new Date();
			todayDate.setDate(todayDate.getDate() + expiredays);
			document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"; 
		}
	
		function closeWin() {
		    if ($("#checkbox").is(":checked")) {
		        setCookie("maindiv", "done", 1);
		    }
		    
		    document.all['divpop'].style.visibility = "hidden";
		}
	</script>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=M+PLUS+1p" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- POPUP Design -->
	<div id="divpop" style="position:absolute; left:1000px; top:130px; z-index:200; visibility:hidden;">
		<table class="table"> 
			<tr> 
			    <td height=360 align=center bgcolor=white> 
			    	<img src="img/popup.png">
			    	
			    	<br>
			    	<br>

			    	<a class="popfonts" href="noticeList">*お知らせ*</a>
			    	<table class="table table-sm popalign">
					    <tr>
							<th class="popfont">更新日時</th>
							<th class="popfont">タイトル</th>
						</tr>
				    	<c:forEach var="notice" items="${noticeList }" end="4">
			    			<tr>
			    				<td>${notice.notice_date }</td>	
			    				<td><a href="noticeDetail?notice_num=${notice.notice_num }">${notice.notice_name }</a></td>	
			    			</tr>
			    		</c:forEach>
			    	</table>
			    </td>
			</tr>
			<tr> 
				<td align=left bgcolor=white>
					<input type="checkbox" id="checkbox" value="checkbox">
					<a class="popupf">今日はもうこのお知らせを見ません。</a> 
					<a class="popupf" href="javascript:closeWin();"><B>[閉じる]</B></a>
			    </td>
			</tr> 
		</table> 
	</div>
	
	<!-- POPUP Cookie -->
	<script>
		var cookiedata = document.cookie;
		
		if (cookiedata.indexOf("maindiv=done") < 0 ) {
		    document.all['divpop'].style.visibility = "visible";
		} else {
			document.all['divpop'].style.visibility = "hidden";
		}
	</script>	
	
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
		<!-- 뒤쪽 어두운 배경 -->
		<div class="overlay overlay-bg"></div>
		
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="banner-content col-lg-9 col-md-12">
					<h1 class="text-uppercase indexjfont">韓国語が学びたい貴方へ。</h1>
					<p class="pt-10 pb-10 indexjfont">
						ケイデュー韓国語エデュケーションサイトは韓国語初心者のための超級の課程から高級レベルの学習資料までを併せる韓国語学習情報を提供します。ケイデュー韓国語エデュケーションサイトは皆の韓国語勉強を応援しています。
					</p>
					<a href="introduceKorean" class="primary-btn text-uppercase">Start!</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start feature Area -->
	<section class="feature-area">
		<div class="container">
			<div class="row">
				<div id="introFeatureArea" class="col-lg-4 single-feature">
					<div class="title indexjfont">
						<h4>ハングルとは？</h4>
					</div>
					<div class="desc-wrap indexjfont">
						<p>千里の道も一歩より始まる!</p>
						<p>学習の前に韓国語について調べてみましょう。</p>
					</div>
				</div>
				<div id="dailyLearningFeatureArea" class="col-lg-4 single-feature">
					<div class="title indexjfont">
						<h4>今日の一言</h4>
					</div>
					<div class="desc-wrap indexjfont">
						<p>塵も積もれば山と成る!</p>
						<p>毎日頑張りましょう。</p>
					</div>
				</div>
				<div id="vocabularyFeatureArea" class="col-lg-4 single-feature">
					<div class="title indexjfont">
						<h4>復習ノート</h4>
					</div>
					<div class="desc-wrap indexjfont">
						<p>私に難しかった単語を一目に!</p>
						<p>完璧になるまで復習してみましょう。</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End feature Area -->

	<!-- Start cta-two Area -->
	<section class="cta-two-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 cta-left"></div>
				<div class="col-lg-4 cta-right"></div>
			</div>
		</div>
	</section>
	<!-- End cta-two Area -->

	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- Script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
		
		// FeatureArea의 각 영역에 클릭 이벤트 추가
		$("#introFeatureArea").click(function () {
			location.href="introduceKorean";
		});
		$("#dailyLearningFeatureArea").click(function () {
			alert("まだ準備中の機能です。\n楽しんで待ってください。");
		});
		$("#vocabularyFeatureArea").click(function () {
			alert("まだ準備中の機能です。\n楽しんで待ってください。");
		});
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>