<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="jp">
<head>
	<title>Login | K.Du</title>
	<meta charset="UTF-8">
	
	<!-- google -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">

	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===================================-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/bootstrap/css/bootstrap.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===================================-->
<!-- 	<link rel="stylesheet" type="text/css" href="resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/animate/animate.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/css-hamburgers/hamburgers.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/animsition/css/animsition.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/select2/select2.min.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/vendor/daterangepicker/daterangepicker.css">
	<!--===================================-->
	<link rel="stylesheet" type="text/css" href="resources/css/util.css">
	<link rel="stylesheet" type="text/css" href="resources/css/main.css">
	<!--===================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-90 p-b-30">
				<span class="login100-form-title p-b-40">
					Login
				</span>
				
				<!-- 구글 로그인 -->
				<c:if test="${sessionScope.loginEmail == null}">
					<div class="g-signin2" data-width="300" data-height="50" data-longtitle="true" data-onsuccess="onSignIn" align="center"></div>
					<br>
				</c:if>
				<c:if test="${sessionScope.loginEmail != null}">
<!-- 					<a href="redirect:/" onclick="signOut();">Sign out</a> -->
					<br>
					<a href="deleteAccount" onclick="signOut();">회원탈퇴</a>
				</c:if>
				
				<div>
				<div class="logback">
				<div class="mt-30 mb-30">
					<a class="pr-30 loginlinks" href="${oldUrl }">
						<span class="fa fa-reply"></span>&nbsp;&nbsp;Back
					</a>
					
					<a class="loginlinks" href="index">
						<span class="fa fa-home"></span>&nbsp;&nbsp;Home
					</a>
					</div>
					</div>
					
<!-- 					<a href="index" class="txt3 bo1 hov1"> -->
<!-- 						test -->
<!-- 					</a> -->
					
<!-- 					<br> -->
					
<!-- 					<a href="index" class="txt3 bo1 hov1"> -->
<!-- 						Home -->
<!-- 					</a> -->
				</div>
				
				<div class="flex-col-c p-t-120">
					<span class="txt2 p-b-10">
						Administrator account
					</span>

					<a href="administrators" class="txt3 bo1 hov1">
						Log in
					</a>
				</div>
			</div>
		</div>
	</div>
	
	<!--===================================-->
	<script src="resources/vendor/animsition/js/animsition.min.js"></script>
	<!--===================================-->
	<script src="resources/vendor/bootstrap/js/popper.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===================================-->
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<!--===================================-->
	<script src="resources/vendor/select2/select2.min.js"></script>
	<!--===================================-->
	<script src="resources/vendor/daterangepicker/moment.min.js"></script>
	<script src="resources/vendor/daterangepicker/daterangepicker.js"></script>
	<!--===================================-->
	<script src="resources/vendor/countdowntime/countdowntime.js"></script>
	<!--===================================-->
	<script src="resources/js/main.js"></script>
</body>
</html>