<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Logout | K.Du</title>
	
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	
	<!-- google -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<script src="https://apis.google.com/js/platform.js" async defer></script>

	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>

	<!-- google 로그인 -->
	<script>
		function onSignIn(googleUser) {
			var profile = googleUser.getBasicProfile();
			console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
			console.log('Name: ' + profile.getName());
			console.log('Image URL: ' + profile.getImageUrl());
			console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
		}
		
		function signOut() {
// 			$('#googleLoginEmail').val();
			var auth2 = gapi.auth2.getAuthInstance();
			
			auth2.signOut().then(function () {
				console.log('User signed out.');
			});
		}
	</script>
</head>
<body>
	<!-- 구글 로그인 -->
	<div class="g-signin2" data-onsuccess="onSignIn"></div>
	<br>
	<br>
	<a href="#" onclick="signOut();">Sign out</a>
</body>
</html>