<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ニックネーム登録 | K.Du</title>
	
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	
	<style>
		div#message {
			color: red;
		}
	</style>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--================================-->	
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/bootstrap/css/bootstrap.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--================================-->	
	<!-- 	<link rel="stylesheet" type="text/css" href="resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/animate/animate.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/css-hamburgers/hamburgers.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/animsition/css/animsition.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/select2/select2.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/daterangepicker/daterangepicker.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/css/util.css">
	<link rel="stylesheet" type="text/css" href="resources/css/main.css">
	<!--================================-->	
	
	<script src="js/vendor/jquery-3.3.1.min.js"></script>

	<!-- 
		로그인 시 아이디와 비밀번호를 요구에 맞게  입력했는지 검증
		해당 아이디나 비밀번호가 없을 경우 출력메시지
	 -->
	 
	<script>
		$(function() {
	// 		$('#message').html(' ');
			$('#joinbtn').on('click', check);
		});
		
		function check() {
			var member_nickname = $('#member_nickname').val();
				
			if(member_nickname.length == 0) {
				alert("ニックネームを入力してください。");
				return;
			}
	// 			if(member_nickname.length < 3 || member_nickname.length > 10){
	// 				$('#message').html('가입을 할 수 없습니다.');
	// 				alert("닉네임이 너무 짧거나 깁니다.");
	// 				return;
	// 			}
				
	// 		$('#message').html(' ');
			$('#joinmember').submit();
		};
		
		function nicknameCheck() {
			window.open("nicknameCheck", "checkWin", "top=200, left=300, height=400, width=500");
		}
	</script>
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-90 p-b-30">
				<span class="login100-form-title p-b-40">
					Nickname
				</span>
	
<%-- 				<div id="message">${message }</div> --%>

				<form class="login100-form validate-form" id="joinmember" action="joinmember" method="get">
					<div class="nickcenter">
						<div class="limit">
							<span class="tt">
								E-Mail
							</span>
							<div class="emailbox">		
								<input class="input101" type="text" id="member_email" name="member_email" value="${member_email}" readonly="readonly">
							</div>
							
							<br>
							
							<span class="tt">
								Nickname
							</span>
							<div class="emailbox">
								<input class="input101" type="text" id="member_nickname" name="member_nickname" placeholder="重複確認をしてください。" readonly="readonly"/>
								<span class="buto">
									<input class="genric-btn primary small" type="button" value="重複確認" onclick="nicknameCheck()">
								</span>
							</div>
							
							<br>
							
							<div>
								<input class="genric-btn primary circle small" id="joinbtn" type="button" value="登録" />
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!--================================-->	
	<script src="resources/vendor/animsition/js/animsition.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/bootstrap/js/popper.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/select2/select2.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/daterangepicker/moment.min.js"></script>
	<script src="resources/vendor/daterangepicker/daterangepicker.js"></script>
	<!--================================-->	
	<script src="resources/vendor/countdowntime/countdowntime.js"></script>
	<!--================================-->	
	<script src="resources/js/jquery-ui.js"></script>
	<!--================================-->	
	<script src="resources/js/main.js"></script>
	<!--================================-->	
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/superfish.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
</body>
</html>
