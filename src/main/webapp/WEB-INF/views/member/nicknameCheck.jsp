<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ニックネーム重複確認 | K.Du</title>
	
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--================================-->	
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/bootstrap/css/bootstrap.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--================================-->	
	<!-- 	<link rel="stylesheet" type="text/css" href="resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/animate/animate.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/css-hamburgers/hamburgers.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/animsition/css/animsition.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/select2/select2.min.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/vendor/daterangepicker/daterangepicker.css">
	<!--================================-->	
	<link rel="stylesheet" type="text/css" href="resources/css/util.css">
	<link rel="stylesheet" type="text/css" href="resources/css/main.css">
	<!--================================-->	
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-90 p-b-30">
				<span class="login100-form-title p-b-40">
					Nickname Search
				</span>

				<!-- 검색 폼 -->
				<form id="nicknameCheckForm" action="nicknameCheck" method="post">
					<div class="nickcheck">
						<span class="checkbox">
							<input class="input102" type="text" name="member_nickname" id="member_nickname"
								value="${nickname }" placeholder="2~10字で入力してください。">
						</span>
						<span>
							<input id="nicknameCheckButton" class="genric-btn primary small" type="submit" value="検索">
						</span>
					</div>
				</form>
				
				<div id="nickcheckResult">
				</div>
				
				<c:if test="${pageContext.request.method eq 'POST' and member eq null }">
					<script>
						var resultTag = "";
						resultTag += '<div class="aa">使用できます。</div>';
						resultTag += '<br>';
						resultTag += '<div class="btnloca">';
						resultTag += '<input class="genric-btn primary small" type="button" value="これにする" onclick="selectmember_nickname(' + "'${nickname }'" + ')">';
						resultTag += '</div>';
						
						$("#nickcheckResult").html(resultTag);
					</script>
				</c:if>

				<c:if test="${pageContext.request.method eq 'POST' and member ne null }">
					<script>
						var errorMessage = "既に登録されているニックネームです。";
						var resultTag = '<div class="aa">' + errorMessage + '</div>';
						$("#nickcheckResult").html(resultTag);
					</script>
				</c:if>
			</div>
		</div>
	</div>
	
	<script>
		// 닉네임 확인 버튼의 이벤트 등록 - submit 체크
		$("#nicknameCheckForm").submit(function () {
			var nickname = $("#member_nickname");
			var errorMessage = "";
			var resultTag = "";
			
			if (nickname.val().trim().length == 0) {
				errorMessage = "ニックネームを入力してください。";
			} else if (nickname.val().length < 2 || nickname.val().length > 10) {
				errorMessage = "2~10字で入力してください。";
			} else if (nickname.val().indexOf("管理者") != -1) {
				errorMessage = "ニックネームで「管理者」は使えません。";
			}
			
			// 문제가 있을 시, 에러 텍스트 출력
			if (errorMessage != "") {
				resultTag += '<div class="aa">' + errorMessage + '</div>';
				$("#nickcheckResult").html(resultTag);
				
				return false;
			}
			
			return true;
		});
		
		function selectmember_nickname(nickname) {
			opener.document.getElementById("member_nickname").value = nickname;
			this.close();
		}
	</script>
	
	<!--================================-->	
	<script src="resources/vendor/animsition/js/animsition.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/bootstrap/js/popper.js"></script>
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/select2/select2.min.js"></script>
	<!--================================-->	
	<script src="resources/vendor/daterangepicker/moment.min.js"></script>
	<script src="resources/vendor/daterangepicker/daterangepicker.js"></script>
	<!--================================-->	
	<script src="resources/vendor/countdowntime/countdowntime.js"></script>
	<!--================================-->	
	<script src="resources/js/jquery-ui.js"></script>
	<!--================================-->	
	<script src="resources/js/main.js"></script>
	<!--================================-->	
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/superfish.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
</body>
</html>
