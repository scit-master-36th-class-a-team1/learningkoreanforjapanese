<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Notice | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC:300,400" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Notice</h1>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start post-content Area -->
	<section class="post-content-area single-post-area">
		<div class="container">
			<div class="row align-items-center text-center">
				<div class="col-lg-8 posts-list boardDatail">
					<div class="single-post row">
						<div class="col-lg-12"></div>
						<div class="col-lg-3  col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6">
									<a class="jpfonts">${nickName }</a> <span class="lnr lnr-user"></span>
								</p>
								<p class="date col-lg-12 col-md-12 col-6">
									<a>${notice.notice_date }</a> <span class="lnr lnr-calendar-full"></span>
								</p>
								<p class="view col-lg-12 col-md-12 col-6">
									<a>${notice.notice_hitcount }</a> <span class="lnr lnr-eye"></span>
								</p>
							</div>
						</div>
						
						<div class="col-lg-9 col-md-9">
							<h3 class="mt-20 mb-20 indexjfont">${notice.notice_name }</h3>

							<div class="row mt-30 mb-30">
								<div class="col-lg-12 mt-30">
									<p class="text-left jpfonts" id="hi">${notice.notice_content }</p>
								</div>
							</div>
							
							<div class="navigation-area">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center"></div>
									<div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center"></div>
								</div>
							</div>
							
							<div class="text-right">
								<!-- 운영자로 로그인 되어있을 때만 글 수정과 삭제 가능 -->
								<c:if test="${!empty sessionScope.administrator_id }">
									<input id="updateButton" class="genric-btn primary small kofonts" type="button" value="수정" />
									&nbsp;&nbsp;
									<input id="deleteButton" class="genric-btn primary small kofonts" type="button" value="삭제" />
								</c:if>
								
								<div class="mt-10">
									<a class="linkcss jpfonts" href="noticeList">リストに戻る>></a>
								</div>
							</div>
						</div>								
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End post-content Area -->

	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
		
		$("#updateButton").click(function () {
			location.href='noticeUpdate?notice_num=${notice.notice_num }';
		});
		$("#deleteButton").click(function () {
			var result = confirm('정말로 삭제하시겠습니까?');
			
			// 확인 버튼을 누르면 해당 공지글 삭제
			if (result) {
				location.href='noticeDelete?notice_num=${notice.notice_num }';
			}
		});
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>