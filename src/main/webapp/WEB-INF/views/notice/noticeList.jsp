<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Notice | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC:300,400" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content blog-header-content col-lg-12">
					<h1 class="text-white">
						Notice		
					</h1>	
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start top-category-widget Area -->
	<section class="top-category-widget-area pt-90 pb-90 ">
	</section>
	<!-- End top-category-widget Area -->
	
	<!-- Start post-content Area -->
	<section class="post-content-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 posts-list">
					<!-- noticeList가 비어있을 때의 출력 -->
					<c:if test="${empty noticeList }">
						<c:if test="${totalCount eq 0 }">
							<div>
								<h3>登録されたお知らせがありません。</h3>
							</div>
						</c:if>
						<c:if test="${totalCount > 0 }">
							<div>
								<h3>検索されたお知らせがありません。</h3>
							</div>
						</c:if>
					</c:if>
					
					<!-- noticeList가 존재할 때의 출력 -->
					<c:if test="${!empty noticeList }">
						<!-- 게시글 출력 반복 -->
						<c:forEach var="notice" items="${noticeList }" varStatus="stat">
							<div class="single-post row">
								<div class="col-lg-3  col-md-3 meta-details">
									<div class="user-details row">
										<p class="user-name col-lg-12 col-md-12 col-6"><a>${stat.count + navi.startRecord }</a> <span class="lnr lnr-pushpin"></span></p>
										<p class="user-name col-lg-12 col-md-12 col-6"><a>${notice.nickName }</a> <span class="lnr lnr-user"></span></p>
										<p class="date col-lg-12 col-md-12 col-6"><a>${notice.notice_date }</a> <span class="lnr lnr-calendar-full"></span></p>
										<p class="view col-lg-12 col-md-12 col-6"><a>${notice.notice_hitcount }</a> <span class="lnr lnr-eye"></span></p>						
									</div>
								</div>
								<div class="col-lg-9 col-md-9 ">
									<a class="posts-title indexjfont" href="noticeDetail?notice_num=${notice.notice_num }"><h3>${notice.notice_name }</h3></a>
<!-- 									<p class="excert"> -->
<!-- 									</p> -->
									<a href="noticeDetail?notice_num=${notice.notice_num }" class="primary-btn">View More</a>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
				
				<div class="col-lg-4 sidebar-widgets">
					<div class="widget-wrap">
						<div class="single-sidebar-widget search-widget">
							<form class="search-form" id="search" action ="noticeList" method="get">
								<div class="default-select" id="default-select">
									<select name="searchItem"> 
										<option value="notice_name" 		${searchItem == 'notice_name' 		? 'selected' : '' }>タイトル</option>
										<option value="administrator_nickname" 	${searchItem == 'administrator_nickname' 	? 'selected' : '' }>ニックネーム</option>
										<option value="notice_content" 		${searchItem == 'notice_content' 	? 'selected' : '' }>内容</option>
									</select>
								</div>
								
								<br>
	                            
	                            <input placeholder="Search Posts" type="text" name="searchWord" value="${searchWord }" >
	                            <button type="submit">
	                            	<i class="fa fa-search">
	                            </i></button>
	                        </form>
						</div>
						
						<!-- 운영자로 로그인 되어있을 때만 글쓰기 가능 -->
						<c:if test="${!empty sessionScope.administrator_id }">
							<div class="single-sidebar-widget user-info-widget">
								<a href="noticeRegist" class="genric-btn primary e-large">신규 공지 등록</a>
							</div>
						</c:if>
					</div>
				</div>
			</div>
			
			<!--페이지-->									
			<nav id="navigator" class="blog-pagination justify-content-center d-flex">
				<ul class="pagination">
					<li class="page-item">
						<a href="noticeList?currentPage=${navi.currentPage - 1}&searchItem=${searchItem }&searchWord=${searchWord }" class="page-link" aria-label="Previous">
							<span aria-hidden="true">
								<span class="lnr lnr-chevron-left"></span>
							</span>
						</a>
					</li>
					
					<c:forEach var="page" begin="${navi.startPageGroup }" end="${navi.endPageGroup }">
						<c:if test="${page != navi.currentPage}">
							<li class="page-item">
								<a class="page-link" href="noticeList?currentPage=${page }&searchItem=${searchItem }&searchWord=${searchWord }">${page }</a>
							</li>
						</c:if>
						<c:if test="${page == navi.currentPage}">
							<li class="page-item active">
								<a href="#" class="page-link">${page }</a>
							</li>
						</c:if>
					</c:forEach>
					
					<li class="page-item">
						<a href="noticeList?currentPage=${navi.currentPage + 1 }&searchItem=${searchItem }&searchWord=${searchWord }" class="page-link" aria-label="Next">
							<span aria-hidden="true">
								<span class="lnr lnr-chevron-right"></span>
							</span>
						</a>
					</li>
				</ul>
			</nav>
		</div>	
	</section>
	<!-- End post-content Area -->
			
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>