<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>     
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Notice | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->
	
	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Notice</h1>
					<p class="text-white link-nav">
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->
	
	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<div class="center">
					<div class="col-lg-8 col-md-8 move">
						<h3 class="mb-30">공지글 수정</h3>
						<form id="noticeUpdateForm" action="noticeUpdate" method="post">
							<input type="hidden" name="notice_num" value="${notice.notice_num }">
							
							<div class="mt-10">
								<input type="text" name="notice_name" class="single-input" value="${notice.notice_name }">
							</div>
							
<%-- 							<div class="mt-10">${sessionScope.loginName }</div> --%>

							<div class="mt-10">
								<div class="single-input">
									${notice.nickName }
									
									<!-- 게시글 수정 후, 관리자 닉네임 변경을 위한 hidden input -->
									<input type="hidden" name="administrator_id" value="${sessionScope.administrator_id }">
								</div>
							</div>
							
							<div class="mt-10">
								<div class="single-input">
									${notice.notice_date }
								</div>
							</div>
							
							<div class="mt-10">
								<textarea name="notice_content" class="single-textarea">
									${notice.notice_content }
								</textarea>
							</div>
							
							<div class="text-right">
								<div class="text-right mt-10">
									<input class="genric-btn primary circle" type="submit" value="글수정" />
									&nbsp;&nbsp;
									<input id="cancelButton" class="genric-btn primary circle" type="button" value="취소" />
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- jquery -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	
	<!-- script -->
	<script src="ckeditor/ckeditor.js"></script>
	<script src="resources/js/custom-ckeditor.js"></script>
	<script>
		// 특정 form의 textarea에 custom ckeditor를 연결함
		addCustomCKEditor("noticeUpdateForm", "notice_content");
		
		// 페이지 기본 구조 로드
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
		
		// 취소버튼 이벤트 추가
		$("#cancelButton").click(function () {
			var result = confirm('정말로 그만두시겠습니까?\n작성 중이던 내용은 저장되지 않습니다.');
			
			// 확인 버튼을 누르면 공지글 목록으로 돌아감
			if (result) {
				location.href = 'noticeList';
			}
		});
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>