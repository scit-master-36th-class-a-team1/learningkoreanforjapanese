<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>ハングルとは? | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC:300,400" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>

	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">ハングルとは?</h1>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<h3 class="mb-30 jpfonts">ハングルの成り立ち</h3>
				<div class="row">
					<div class="col-md-3">
						<img src="img/introduceKorea/king-sejong.jpg" alt="" class="img-fluid">
					</div>
					<div class="col-md-9 mt-sm-20 left-align-p">
						<p class="jpfonts">ハングルは1443年、朝鮮王朝(1392～1910)第4代目の王｢世宗大王[セジョンデワン]｣時代に作られ、1446年に｢訓民正音[フンミンジョンウム]｣の名で公布された独自の文字です。
						한글[ハングル]の한[ハン]は｢偉大な、大きな、１つの｣という意味合いで、글[グル]は｢文字｣という意味です。</p>
						<br>
						<p class="kofonts">한글은 1443년, 조선 왕조(1392~1910) 제4대 왕 세종대왕 시대에 만들어지고 1446년에 훈민정음의 이름으로 공포된 독자의 문자입니다.
						한글의 '한'은 [위대한, 큰, 1개]라는 의미이고 '글'은 [문자]라는 의미입니다.
						</p>
					</div>
				</div>
			</div>
			<div class="section-top-border text-right">
				<h3 class="mb-30 jpfonts">言語の呼称について</h3>
				<div class="row">
					<div class="col-md-9">
						<p class="text-left jpfonts">
							1950年6･25朝鮮戦争以来、朝鮮半島が南北に分断され、北側の｢朝鮮民主主義人民共和国｣では｢朝鮮語｣、南側の｢大韓民国｣では｢韓国語｣と呼ばれています。	
						</p>
						<br>
						<p class="text-left kofonts">
							1950년 6.25한국 전쟁 이후, 한반도가 남북으로 분단되고 북측의 '조선민주주의 인민 공화국'에서는 [조선어], 남측의 '대한민국'에서는 [한국어]로 불리고 있습니다.
						</p>
					</div>
					<div class="col-md-3">
						<img src="resources/img/introduceKorea/conflict.jpg" alt="" class="img-fluid">
					</div>
				</div>
			</div>
			<div class="section-top-border">
				<h3 class="mb-30 jpfonts">このウェブサイトは</h3>
				<div class="row">
					<div class="col-md-3">
						<img src="img/introduceKorea/young-woman.jpg" alt="" class="img-fluid">
					</div>
					<div class="col-md-9 mt-sm-20 left-align-p">
						<p class="jpfonts">
							当サイトでは韓国語を簡単に紹介します。韓国語と日本語は、びっくりするほど似ています。漢字熟語があったり、敬語表現があったり、助詞もあれば語順もいっしょ。
							このページを見て韓国語に興味を持っていただくきっかけになればと思い、まったく何も知らない方でもゼロから学べるように構成しました。
							それでは、始め見ましょうか。
						</p>
						<br>
						<p class="kofonts">
							본 사이트에서는 한국어를 쉽게 소개하고 있습니다. 한국어와 일본어는 놀라울 정도로 닮았습니다. 한자 숙어가 있거나, 경어 표현이 있거나, 조사도 있으며 어순도 같습니다.
							이 페이지를 보고 한국어에 흥미를 가져다 주는 계기가 되었으면 하고 생각하고 전혀 모르는 분이라도 처음부터 배울 수 있도록 구성했습니다. 그럼 시작해볼까요?
						</p>
					</div>
				</div>	
			</div>
			
			<section class="button-area">
				<div class="container border-top-generic text-center">
					<div class="button-group-area">
						<input class="genric-btn primary" type="button" value="学習の開始" onclick="location.href='http://localhost:8888/educationweb/studyIndex?study_type=level1'">
					</div>
				</div>
			</section>	
		</div>
	</div>
	<!-- End Align Area -->

	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- Script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>