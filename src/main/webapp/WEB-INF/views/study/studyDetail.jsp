<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    	

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Education ${study_type } | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC:300,400" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	
	<script>
		var study_num = '${study.study_num}'; //게시글 번호
		
		$(function() {
			init();
			
			$("#commentInsertForm").submit(function (event) { // 댓글 등록 버튼 클릭시 	
				event.preventDefault();
			
// 				alert("administrator_id : ${sessionScope.administrator_id }");
// 				alert("loginEmail : ${sessionScope.loginEmail }");
				
				if (${sessionScope.administrator_id == null && sessionScope.loginEmail == null }) {
					alert("先にログインをしてください。");
					location.href='login';
					return;
				}
				
			    if ($("#study_comment").val() == "") {
					alert("内容を入力してください。");
					return;
			    }
				
			    var insertData = $("#commentInsertForm").serialize(); // commentInsertForm의 내용을 가져옴
			    
			    commentInsert(insertData); // Insert 함수호출(아래)
			});
		});
		
		// 모든 단어 저장소에서 읽어옴
		function init() {
			var study_num = $("#study_num").val();
	// 		alert(study_num);
	
			$.ajax({
				url: "vocaList"
				, type: "get"
				, data: {'study_num' : study_num}
				, success: function (vList) {
					output(vList);
				}
			});
		}
		
		function output(resp) {
			var data = '';
			
			$.each(resp, function (index, item) {
				createFile(item.voca_ko);
				data += '<tr><td class="kofonts tablehw voca_ko" >'+ item.voca_ko;
				data += '</td>';
				data += '<td class="jpfonts tablehw voca_jp" >'+ item.voca_jp ;
				data += '</td>';
				data += '<td class="jpfonts tablehw voca_yomi" >'+ item.voca_yomi ;
				data += '</td>';
				data += '<td class="tablesimbol" ><span class="lnr lnr-volume-medium"></span>';
				data += '</td>';
				data += '<td class="tablehw"><input type="button" class ="genric-btn success radius small TTSbtn jpfonts" data-sno="'+ item.voca_ko;
				data += '"value="発音を聞く" /></td></tr>';
			});
			
			$("#printHere").append(data);
			$(".TTSbtn").on("click", function (event){
				TTS($(event.target));
			});
		}
		
		// 댓글 목록 
		function commentList() {
		    $.ajax({
		        url : 'commentList'
		        , type : 'get'
				, data : {'study_num':study_num}
				, success : function (data) {
		            var a = ''; 
		            var loginemail = '${sessionScope.loginEmail}';
		            var administrator_id ='${sessionScope.administrator_id}'

		            $.each(data, function(key, value) { 
		                a += '<div style="border-bottom:1px solid darkgray; margin-bottom: 15px;">';
		                a += '<div id="commentInfo'+value.study_comment_num+'"><span class="commentnick indexjfont">'+value.nickName+'</span><span class="commentdate">'+value.study_comment_date+'</span>';
		                a += '<div class="contentsboxx" id="commentContent'+value.study_comment_num+'"><p class="jpfonts">'+value.study_comment;
		                if (loginemail == value.member_email || administrator_id != '' ) {
			                a += '<div class="text-right" id="commentUpdate'+value.study_comment_num+'"><a class="contentbnt mr-10 kofonts" onclick="commentUpdate('+value.study_comment_num+',\''+value.study_comment+'\');">修正</a>';
			                a += '<a class="contentbnt kofonts" onclick="commentDelete('+value.study_comment_num+');">削除</a></div>';
		                }
		                a += '</div></div></div>';
		                
		            });
		            
		            $(".commentList").html(a);
		        }
		    });
		}
		 
		// 댓글 등록
		function commentInsert(insertData) {
		    $.ajax({
		        url : 'commentInsert'
		        , type : 'get'
		        , data : insertData
		        , success : function (data) {
					if (data == 1) {
						commentList(); //댓글 작성 후 댓글 목록 reload
						$('#study_comment').val('');
						
						alert("登録完了。");
		            }
		        }
		    });
		}
		
		// 댓글 수정 - 댓글 내용 출력을 input 폼으로 변경 
		function commentUpdate(study_comment_num, study_comment){
		    var a ='';
		    
		    a += '<div class="input-group">';
		    a += '<input type="text" class="form-control" id="content_'+study_comment_num+'" value="'+study_comment+'"/>';
		    a += '<span class="ml-10 input-group-btn"><button class="genric-btn primary circle" type="button" onclick="commentUpdateProc('+study_comment_num+');">修正</button></span>';
		    a += '</div>';
		    a += '<div class="text-right"><a class="contentbnt mr-10 kofonts" onclick="commentList()">Cancel</a>';
		    a += '</div>'
		    $('#commentContent'+study_comment_num).html(a);
		    
		}
		 
		// 댓글 수정
		function commentUpdateProc(study_comment_num) {
		    var updateContent = $("#content_" + study_comment_num).val();
		    
		    if (updateContent == "") {
				alert("内容を入力してください。");
				return;
		    }
		    
		    $.ajax({
		        url : 'commentUpdate'
		        , type : 'get'
		        , data : {'study_comment' : updateContent, 'study_comment_num' : study_comment_num}
		    	, success : function (data) {
		            if (data == 1) commentList(study_num); // 댓글 수정후 목록 출력 
		        }
		    });
		}
		 
		// 댓글 삭제 
		function commentDelete(study_comment_num) {
			var result = confirm('本当に削除しますか?');
			
			// 취소 버튼을 누르면 삭제 미진행
			if (!result) {
				return;
			}
			
			// 삭제 진행
		    $.ajax({
		        url : 'commentDelete'
		        , type : 'get'
		        , data : {'study_comment_num':study_comment_num}
		    	, success : function (data) {
		            if(data == 1) commentList(study_num); // 댓글 삭제후 목록 출력 
		        }
		    });
		}
		
		// TTS 기능
		function TTS(target) {
			var text = $(target).attr('data-sno');
			$("#audioName").val(text);
				
			var a = '<audio id="myAudio" controls>';
			a += '<source src="/mp3/';
			a += text;
			a += '.mp3';
			a += '" type="audio/mpeg">';
			a += '</audio>';
				
			$("#printAudio").html(a);
		}
		
		// TTS 기능
		function createFile(text){
			$.ajax({
				url : 'speak'
				, type : 'get'
				, data : { 'text' : text}
				, success : function () {
				}
			});
		}
		 
		$(document).ready(function () {
		    commentList(); // 페이지 로딩시 댓글 목록 출력 
		});
	</script>
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->
	
	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Education ${study_type }</h1>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->
	
	<!-- 글 번호 임시 저장 -->
	<input type="hidden" id="study_num" value=${study.study_num }>
	
	<!-- Start Main area -->
	<section class="post-content-area single-post-area">
		<div class="container">
			<div class="row align-items-center text-center">
				<div class="col-lg-8 posts-list boardDatail">
					<div class="single-post row">
						<div class="col-lg-12">							
						</div>
						
						<div class="col-lg-3  col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6">
									<a>${study.study_type }</a>
									<span class="lnr lnr-pencil"></span>
								</p>
								<p class="date col-lg-12 col-md-12 col-6">
									<a class="jpfonts">${study.study_chapter }</a>
									<span class="lnr lnr-bookmark"></span>
								</p>																			
							</div>
						</div>
						
						<div class="col-lg-9 col-md-9">
							<h3 class="mt-20 mb-20 indexjfont">${study.study_mainTitle }</h3>
							<p class="mt-15 text-left textbold excert indexjfont">
								${study.study_subTitle }
							</p>
							<div class="text-left mt-30 indexjfont">
								${study.study_content }
							</div>
							
							<!-- Start Audio Area -->
							<div>
								<div class="mt-50 text-left">
<!-- 								띄어쓰기 -->
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
									<input class="input11" id="audioName" readonly="readonly" type="text" value="" name="audioName" placeholder="単語を選んで聞いてみましょう！">
								</div>
								<div class="mt-10 text-left">
									<div id="printAudio">
										<audio id="myAudio" controls>
											<source src="#" type="audio/mpeg">
										</audio>
									</div>
								</div>
							</div>
							<!-- End Audio Area -->
							
							<br>
							
							<!-- Start Voca area -->
							<div class="mt-15 generic-blockquote">
								<table class="mt-15 text-left" id="printHere"></table>
							</div>
							<!-- End Voca area -->
							
							<br>
							
							<c:if test="${sessionScope.administrator_id != null}">
								<div class="mt-20 text-right">
									<input id="updateButton" class="genric-btn primary small" type="button" value="수정" />
									&nbsp;&nbsp;
									<input id="deleteButton" class="genric-btn primary small" type="button" value="삭제" />
								</div>
							</c:if>
							
							<div class="mt-10 text-right">
								<a class="linkcss" href="studyIndex?currentPage=${navi.currentPage}&searchItem=${searchItem }&searchWord=${searchWord}&study_type=${study_type}">リストに戻る>></a>	
							</div>
						</div>
					</div>
					
					<!-- 댓글 폼 -->
					<div class="mt-50 comentbox1 container">
						<label class="comen" for="content">Comment</label>
						
						<form id="commentInsertForm">
							<div class="mt-20 input-group">
					 			<input type="hidden" id="study_num" name="study_num" value="${study.study_num}"/>
 								<input type="text" class="form-control" id="study_comment" name="study_comment" placeholder="内容を入力してください。">
				 				<span class="ml-10 input-group-btn">
	               					<button type="submit" class="genric-btn primary circle">登録</button>
             					</span>
           					</div>
     					</form>
  					</div>
					
					<!-- 입력한 댓글 창 출력 -->
					<div class="mt-20 comentbox1 text-left container">
    					<div class="commentList"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Main area -->
	
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- Script -->
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
		
		$("#updateButton").click(function () {
			location.href='studyUpdate?study_num=${study.study_num}'
		});
		$("#deleteButton").click(function () {
			var result = confirm('정말로 삭제하시겠습니까?');
			
			// 확인 버튼을 누르면 해당 학습글 삭제
			if (result) {
				location.href='studyDelete?study_num=${study.study_num}&study_type=${study.study_type}';
			}
		});
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>