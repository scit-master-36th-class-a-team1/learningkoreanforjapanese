<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Education ${study_type } | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC:300,400" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Gothic+A1" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Education ${study_type }
					</h1>
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area -->	

	<!-- Start top-category-widget Area -->
	<section class="top-category-widget-area pt-90 pb-90 ">
	</section>
	<!-- End top-category-widget Area -->
	
	<!-- Start post-content Area -->
	<section class="post-content-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 posts-list">
					<!-- studyList가 비어있을 때의 출력 -->
					<c:if test="${empty studyList }">
						<c:if test="${totalCount eq 0 }">
							<div>
								<h3>登録された講義がありません。</h3>
							</div>
						</c:if>
						<c:if test="${totalCount > 0 }">
							<div>
								<h3>検索された講義がありません。</h3>
							</div>
						</c:if>
					</c:if>
					
					<!-- studyList가 존재할 때의 출력 -->
					<c:if test="${!empty studyList }">
						<!-- 게시글 출력 반복 -->
						<c:forEach var="study" items="${studyList }">
							<div class="single-post row">
								<div class="col-lg-3  col-md-3 meta-details">
								</div>
								<div class="col-lg-9 col-md-9 ">
									<a class="posts-title indexjfont" href="studyDetail?currentPage=${navi.currentPage}&searchItem=${searchItem}&searchWord=${searchWord}&study_num=${study.study_num}&study_type=${study_type}">
										<h3>${study.study_mainTitle}</h3>
									</a>
									<p class="excert indexjfont">
										${study.study_subTitle }
									</p>
									<a href="studyDetail?currentPage=${navi.currentPage}&searchItem=${searchItem}&searchWord=${searchWord}&study_num=${study.study_num}&study_type=${study_type}" class="primary-btn">
										View More
									</a>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
				
				<div class="col-lg-4 sidebar-widgets">
					<div class="widget-wrap">
						<div class="single-sidebar-widget search-widget">
							<form class="search-form" id="search" action ="studyIndex" method="get">
								<div class="default-select jpfonts" id="default-select">
									<select name="searchItem">
										<option value="study_mainTitle" ${searchItem=='study_mainTitle' ? 'selected' : '' }>タイトル</option>
										<option value="study_subTitle" ${searchItem=='study_subTitle' ? 'selected' : '' }>サブタイトル</option>
									</select>
								</div>
								
								<br>
	                            
	                            <input placeholder="Search Posts" type="text" name="searchWord" value="${searchWord }" >
	                            <input type="hidden" name="study_type" value="${study_type }">
	                            
	                            <button type="submit">
	                            	<i class="fa fa-search"></i>
	                            </button>
	                        </form>
						</div>
						
						<!-- 운영자로 로그인 되어있을 때만 글쓰기 가능 -->
						<c:if test="${sessionScope.administrator_id != null}">
							<div class="single-sidebar-widget user-info-widget">
								<a href="studyRegist?requestStudyType=${study_type }" class="genric-btn primary e-large">신규 학습 등록</a>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		
		<!--페이지-->									
 		<nav id="navigator" class="blog-pagination justify-content-center d-flex">
			<input type="text" id = "study_type" name="study_type" hidden="hidden" value="${study_type}" >
			<ul class="pagination">
				<li class="page-item">
					<a href="studyIndex?currentPage=${navi.currentPage-1}&searchItem=${searchItem }&searchWord=${searchWord}&study_type=${study_type}" class="page-link" aria-label="Previous">
						<span aria-hidden="true">
							<span class="lnr lnr-chevron-left"></span>
						</span>
					</a>
				</li>
				
				<c:forEach var="page" begin="${navi.startPageGroup }" end="${navi.endPageGroup }">
					<c:if test="${navi.currentPage==page }">
						 <li class="page-item active"><a href="#" class="page-link">${page }</a></li>
					</c:if>
				
					<c:if test="${navi.currentPage!=page }">
						<li class="page-item"><a href="studyIndex?currentPage=${page }&searchItem=${searchItem }&searchWord=${searchWord}${searchWord}&study_type=${study_type}" class="page-link">${page}</a></li>
					</c:if>
				</c:forEach>
	                        
				<li class="page-item">
					<a href="studyIndex?currentPage=${navi.currentPage+1}&searchItem=${searchItem }&searchWord=${searchWord}${searchWord}&study_type=${study_type}" class="page-link" aria-label="Next">
						<span aria-hidden="true">
							<span class="lnr lnr-chevron-right"></span>
						</span>
					</a>
				</li>
			</ul>
		</nav>
	</section>
	<!-- End post-content Area -->
			
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>