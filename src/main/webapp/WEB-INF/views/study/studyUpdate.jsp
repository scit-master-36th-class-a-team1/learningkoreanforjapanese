<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Update Education | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=M+PLUS+1p" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		// 글 등록 
		function update() {
			var study_num = $("#study_num").val();
			var study_mainTitle = $("#study_mainTitle").val();
			var study_subTitle = $("#study_subTitle").val();
			var study_content = CKEDITOR.instances["study_content"].getData();
			var study_type = $("#study_type").val();
			var study_chapter = $("#study_chapter").val();
			var administrator_id = $("#administrator_id").val();
			
			if(study_mainTitle.length == 0 || study_subTitle.length == 0 || study_type.length == 0 || study_chapter.length == 0 || study_content == 0) {
				alert("데이터를 입력해 주세요");
				return;
			}
	
			var studyData = {
				'study_num' : study_num
				, 'study_mainTitle' : study_mainTitle
				, 'study_subTitle' : study_subTitle
				, 'study_content' : study_content
				, 'study_type' : study_type
				, 'study_chapter' : study_chapter
				, 'administrator_id' : administrator_id
			}
	
			$.ajax({
				url: "studyUpdate"
				, type: "post"
				, data: studyData
				, success: function (resp) {
					alert("등록이 완료되었습니다!");
					location.href='vocaUpdate?study_num=' + resp + '&requestStudyType=' + study_type;
				} 
			});
		}
	</script>
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->
	
	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Update Education
					</h1>
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area -->	
			
	<input type="hidden" id="study_num" name="study_num" value="${study.study_num }">
	<!-- Start regist Area -->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<div class="center">
					<div class="col-lg-8 col-md-8 move">
						<h3 class="mb-30">학습 내용 수정하기</h3>
						
						<form id="studyUpdatetForm" action="">
							<div class="mt-10">
								<input type="text" id="study_mainTitle" name="study_mainTitle" placeholder="제목"
									onfocus="this.placeholder = ''"onblur="this.placeholder = '제목'" required
									class="single-input indexjfont" value="${study.study_mainTitle }">
							</div>
							
							<div class="mt-10">
								<input type="text" id="study_subTitle" name="study_subTitle" placeholder="부제목"
									onfocus="this.placeholder = ''" onblur="this.placeholder = '부제목'" required
									class="single-input indexjfont" value="${study.study_subTitle}">
							</div>
							
							<div class="input-group-icon mt-10">
								<div class="icon">
										<i class="fa fa-pencil" aria-hidden="true"></i>
								</div>
								<div class="form-select" id="default-select2">
									<select id="study_type">
										<option value="level1" ${study.study_type=='level1' ? 'selected' : '' }>level1</option>
										<option value="level2" ${study.study_type=='level2' ? 'selected' : '' }>level2</option>
										<option value="level3" ${study.study_type=='level3' ? 'selected' : '' }>level3</option>
									</select>
								</div>
							</div>
							
							<div class="mt-10">
								<input type="text" id="study_chapter" name="study_chapter" placeholder="study_chapter"
									onfocus="this.placeholder = ''" onblur="this.placeholder = 'study_chapter'" required
									class="single-input indexjfont" value="${study.study_chapter}">
							</div>
							
							<div class="mt-10">
								<!-- 관리자 아이디 전달을 위한 hidden type input -->
								<input type="hidden" id="administrator_id" name="administrator_id"
									value="${sessionScope.administrator_id}">
									
								<!-- 관리자 닉네임 보여주기 -->
								<input type="text" class="single-input indexjfont indexjfont" value="${sessionScope.administrator_nickname}"
									required readonly>
							</div>
							
							<div class="mt-10">
								<textarea name="study_content" class="single-textarea jpfonts" >
									${study.study_content }
								</textarea>
							</div>
						</form>
						
						<div class="mt-20">
							<div class="button-group-area text-right">
								<button onclick="update();" class="genric-btn primary circle">수정</button>
								<button id="cancelButton" class="genric-btn primary circle">취소</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End regist Area -->

	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script src="ckeditor/ckeditor.js"></script>
	<script src="resources/js/custom-ckeditor.js"></script>
	<script>
		// 특정 form의 textarea에 custom ckeditor를 연결함
		addCustomCKEditor("studyUpdateForm", "study_content");
		
		// 페이지 기본 구조 로드
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
		
		// 취소버튼 이벤트 추가
		$("#cancelButton").click(function () {
			var result = confirm('정말로 그만두시겠습니까?\n작성 중이던 내용은 저장되지 않습니다.');
			
			// 확인 버튼을 누르면 공지글 목록으로 돌아감
			if (result) {
				location.href = 'studyIndex?study_type=${study.study_type }';
			}
		});
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>