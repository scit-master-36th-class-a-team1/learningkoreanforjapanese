<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Update Voca | K.Du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=M+PLUS+1p" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$(function() {
			init();
		})
	
		function index() {
			location.href='studyIndex?study_type=${requestStudyType }';
		}
		
		//모든 단어 저장소에서 읽어옴
		function init() {
			var study_num = $("#study_num").val();
			$.ajax({
				url: "vocaList"
				, type: "get"
				, data: {'study_num' : study_num}
				, success: function(vList) {
					output(vList);
				}
			});
		}
		
		function output(resp){
			var data = '';
			$.each(resp, function(index, item){
				data += '<tr><td class="tablehw voca_ko kofonts" >'+ item.voca_ko;
				data += '</td>';
				data += '<td class="tablehw voca_jp jpfonts" >'+ item.voca_jp ;
				data += '</td>';
				data += '<td class="tablehw voca_yomi jpfonts" >'+ item.voca_yomi ;
				data += '</td>';
				data += '<td class="tablehw" ><input type="button"  class="genric-btn success radius small delbtn kofonts" data-sno="'+item.voca_num ;
				data += '"value="삭제" /></td></tr>';
			});
			
			$("#printHere").append(data);
			$(".delbtn").on("click", function(event){
				del($(event.target));
			});
			
			if ($("#printHere > tr").length == 0) {
				$("#vokaTableEmptyMessage").show();
			} else {
				$("#vokaTableEmptyMessage").hide();
			}
		}
		
		// 보카 삭제
		function del(target) {
			var delno = $(target).attr('data-sno');
			if (delno != null) {
				$.ajax({
					url:"vocaDelete"
					, type : "post"
					, data : {voca_num : delno}
					, success: function (result) {
						$("#printHere").empty();
						init();
					}
				});
			} else {
				alert("??");
			}
			
		}
		
		// 보카 등록 
		function addVoca() {
			var voca_ko = $("#voca_ko").val();
			var voca_jp = $("#voca_jp").val();
			var voca_yomi = $("#voca_yomi").val();
			var study_num = $("#study_num").val();
			
			if (voca_ko.length == 0 || voca_jp.length == 0) {
				alert("데이터를 입력해 주세요");
				return;
			}
			
			// 클래스 내용 지우기
			$("#voca_ko").val('');
			$("#voca_jp").val('');
			$("#voca_yomi").val('');
			
			var tempvocaData = {
				'study_num' :study_num
				, 'voca_ko' : voca_ko
				, 'voca_jp' : voca_jp
				, 'voca_yomi' : voca_yomi
			}
	
			$.ajax({
				url: "vocaWrite"
				, type: "post"
				, data: tempvocaData
				, success: function () {
					$("#printHere").empty();
					init();
				}
			});
		}
	</script>
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->
	
	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Update Voca
					</h1>
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Voca Area -->
<!-- 	글 번호 받을 위치	 -->
	<input type="hidden" id="study_num" name="study_num" value="${study_num }">
	
<!-- 	글 번호 받고 난 이후 열어 줄 위치 -->
	
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<div class="center">
					<div class="col-lg-8 col-md-8 move">
						<h3 class="mb-30">단어 수정</h3>
						
						<form>
							<div>
								<div class="mt-10">
									<input type="text" id="voca_ko" name="voca_ko" placeholder="단어 (한글)"
										onfocus="this.placeholder = ''"
										onblur="this.placeholder = '단어 (한글)'" class="single-input kofonts">
								</div>
								<div class="mt-10">
									<input type="text" id="voca_jp" name="voca_jp" placeholder="단어 (일본어)"
										onfocus="this.placeholder = ''"
										onblur="this.placeholder = '단어 (일본어)'" class="single-input jpfonts">
								</div>
								<div class="mt-10">
									<input type="text" id="voca_yomi" name="voca_yomi" placeholder="읽는 법 (가타카나)"
										onfocus="this.placeholder = ''"
										onblur="this.placeholder = '읽는 법 (가타카나)'" class="single-input jpfonts">
								</div>
							</div>
						</form>
						
						<div class="mt-10 text-right">
							<div class="button-group-area">
								<button onclick="addVoca();" class="genric-btn primary small kofonts">추가</button>
							</div>
						</div>
						
						<div class="mt-15 generic-blockquote">
							<table class="mt-15 printHere" id="printHere">
							</table>
							<p id="vokaTableEmptyMessage">단어를 추가해주세요.</p>
						</div>
						
						<div class="text-right mt-10 button-group-area">
							<button onclick="index();" class="genric-btn primary circle">완료</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Voca Area -->
	
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script>
		// 페이지 기본 구조 로드
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>