<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
<script>
var study_num = '${study.study_num}'; //게시글 번호

$(function() {	
	$("[name='commentInsertBtn']").click(function(){ //댓글 등록 버튼 클릭시 
	    var insertData = $('[name=commentInsertForm]').serialize(); //commentInsertForm의 내용을 가져옴
	    commentInsert(insertData); //Insert 함수호출(아래)
	});
})
 
//댓글 목록 
function commentList(){
    $.ajax({
        url : 'commentList',
        type : 'get',
        data : {'study_num':study_num},
        success : function(data){
            var a =''; 
            $.each(data, function(key, value){ 
                a += '<div class="commentArea" style="border-bottom:1px solid darkgray; margin-bottom: 15px;">';
                a += '<div class="commentInfo'+value.study_comment_num+'">'+'댓글번호 : '+value.study_comment_num+' / 작성자 : '+value.practice;
                a += '<a onclick="commentUpdate('+value.study_comment_num+',\''+value.study_comment+'\');"> 수정 </a>';
                a += '<a onclick="commentDelete('+value.study_comment_num+');"> 삭제 </a> </div>';
                a += '<div class="commentContent'+value.study_comment_num+'"> <p> 내용 : '+value.study_comment +'</p>';
                a += '</div></div>';
            });
            
            $(".commentList").html(a);
        }
    });
}
 
//댓글 등록
function commentInsert(insertData){
    $.ajax({
        url : 'commentInsert',
        type : 'get',
        data : insertData,
        success : function(data){
            if(data == 1) {
                commentList(); //댓글 작성 후 댓글 목록 reload
                $('[name=study_content]').val('');
            }
        }
    });
}
 
//댓글 수정 - 댓글 내용 출력을 input 폼으로 변경 
function commentUpdate(study_comment_num, study_comment){
    var a ='';
    
    a += '<div class="input-group">';
    a += '<input type="text" class="form-control" name="content_'+study_comment_num+'" value="'+study_comment+'"/>';
    a += '<span class="input-group-btn"><button class="btn btn-default" type="button" onclick="commentUpdateProc('+study_comment_num+');">수정</button> </span>';
    a += '</div>';
    
    $('.commentContent'+study_comment_num).html(a);
    
}
 
//댓글 수정
function commentUpdateProc(study_comment_num){
    var updateContent = $('[name=content_'+study_comment_num+']').val();
    
    $.ajax({
        url : 'commentUpdate',
        type : 'get',
        data : {'study_comment' : updateContent, 'study_comment_num' : study_comment_num},
        success : function(data){
            if(data == 1) commentList(study_num); //댓글 수정후 목록 출력 
        }
    });
}
 
//댓글 삭제 
function commentDelete(study_comment_num){
    $.ajax({
        url : 'commentDelete',
        type : 'get',
        data : {'study_comment_num':study_comment_num},
        success : function(data){
            if(data == 1) commentList(study_num); //댓글 삭제후 목록 출력 
        }
    });
}
 
$(document).ready(function(){
    commentList(); //페이지 로딩시 댓글 목록 출력 
});
</script>
</head>
<body>
<div class="container">
    <div class="col-xs-12" style="margin:15px auto;">
        <label style="font-size:20px;"><span class="glyphicon glyphicon-list-alt"></span>게시글 상세</label>
    </div>
    
    <div class="col-xs-12">
            <dl class="dl-horizontal">
              <dt>제목</dt>
              <dd>${study.study_mainTitle}</dd>
              
              <dt>작성자</dt>
              <dd>${study.administrator_id}</dd>
              
              <dt>작성날짜</dt>
              <dd>
                  ${study.study_date}
              </dd>
              
              <dt>내용</dt>
              <dd>${study.study_content}</dd>
            </dl>
    </div>
  
    <div class="container">
        <label for="content">comment</label>
        <form name="commentInsertForm">
            <div class="input-group">
               <input type="hidden" name="study_num" value="${study.study_num}"/>
               <input type="text" class="form-control" id="study_comment" name="study_comment" placeholder="내용을 입력하세요.">
               <span class="input-group-btn">
               		<c:choose>
               			<c:when test="${sessionScope.administrator_id != null || sessionScope.loginEmail != null}">
               				<button class="btn btn-default" type="button" name="commentInsertBtn">등록</button>
               			</c:when>
               			<c:when test="${sessionScope.administrator_id == null || sessionScope.loginEmail == null}">
               				<button class="btn btn-default" type="button" disabled="disabled" name="commentInsertBtn">등록</button>
               			</c:when>
               		</c:choose>
               </span>
              </div>
        </form>
    </div>
    
    <div class="container">
        <div class="commentList"></div>
    </div>
</div>
</body>
</html>