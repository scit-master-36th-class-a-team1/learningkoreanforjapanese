<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>エーラー発生 | K.Du</title>
	
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
</head>
<body>
	<div>
		<h2>${errorTitle } a</h2>
		<hr />
		
		<p>${errorMessage } a</p>
	</div>
	<div>
		<a href="${oldUrl }">이전으로</a>
		<a href="index">메인으로</a>
	</div>
</body>
</html>