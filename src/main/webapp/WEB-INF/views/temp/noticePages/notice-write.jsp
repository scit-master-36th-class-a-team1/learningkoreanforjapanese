<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Notice??? | K.du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	
	<!-- CSS -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Notice</h1>
					<p class="text-white link-nav">
						<a href="index.html">Home </a>
						<span class="lnr lnr-arrow-right"></span>
						<a href="elements.html"> Elements</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container">
			<div class="section-top-border">
				<div class="center">
					<div class="col-lg-8 col-md-8 move">
						<h3 class="mb-30">글쓰기</h3>
						<form action="#">
							<div class="mt-10">
								<input type="text" name="name" placeholder="Name"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Name'" required
									class="single-input">
							</div>
							<div class="mt-10">
								<input type="password" name="password" placeholder="password"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'password'" required
									class="single-input">
							</div>
							<div class="mt-10">
								<textarea class="single-textarea" placeholder="Message"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Message'" required></textarea>
							</div>
							<div class="mt-10">
								<input type="file" name="upload" value="파일첨부">
							</div>
							<div class="text-right">
								<a href="#" class="genric-btn primary circle">글 등록</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>