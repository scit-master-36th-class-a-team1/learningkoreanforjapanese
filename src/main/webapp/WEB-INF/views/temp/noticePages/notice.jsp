<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Notice | K.du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative blog-home-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content blog-header-content col-lg-12">
					<h1 class="text-white">
						Notice		
					</h1>	
					<p class="text-white">
						There is a moment in the life of any aspiring astronomer that it is time to buy that first
					</p>
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area				   -->

	<!-- Start top-category-widget Area -->
	<section class="top-category-widget-area pt-90 pb-90 ">
	</section>
	<!-- End top-category-widget Area -->
	
	<!-- Start post-content Area -->
	<section class="post-content-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 posts-list">
					<div class="single-post row">
						<div class="col-lg-3  col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6"><a>작성자</a> <span class="lnr lnr-user"></span></p>
								<p class="date col-lg-12 col-md-12 col-6"><a>2019-03-07</a> <span class="lnr lnr-calendar-full"></span></p>
								<p class="view col-lg-12 col-md-12 col-6"><a>1.2M Views</a> <span class="lnr lnr-eye"></span></p>
								<p class="comments col-lg-12 col-md-12 col-6"><a href="#">06 Comments</a> <span class="lnr lnr-bubble"></span></p>						
							</div>
						</div>
						<div class="col-lg-9 col-md-9 ">
							<a class="posts-title" href="notice-detail"><h3>Astronomy Binoculars A Great Alternative</h3></a>
							<p class="excert">
								MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
							</p>
							<a href="notice-detail" class="primary-btn">View More</a>
						</div>
					</div>
					<div class="single-post row">
						<div class="col-lg-3  col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6"><a href="#">Mark wiens</a> <span class="lnr lnr-user"></span></p>
								<p class="date col-lg-12 col-md-12 col-6"><a href="#">12 Dec, 2017</a> <span class="lnr lnr-calendar-full"></span></p>
								<p class="view col-lg-12 col-md-12 col-6"><a href="#">1.2M Views</a> <span class="lnr lnr-eye"></span></p>
								<p class="comments col-lg-12 col-md-12 col-6"><a href="#">06 Comments</a> <span class="lnr lnr-bubble"></span></p>						
							</div>
						</div>
						<div class="col-lg-9 col-md-9 ">
							
							<a class="posts-title" href="notice-detail"><h3>The Basics Of Buying A Telescope</h3></a>
							<p class="excert">
								MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
							</p>
							<a href="notice-detail" class="primary-btn">View More</a>
						</div>
					</div>
					<div class="single-post row">
						<div class="col-lg-3  col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6"><a href="#">Mark wiens</a> <span class="lnr lnr-user"></span></p>
								<p class="date col-lg-12 col-md-12 col-6"><a href="#">12 Dec, 2017</a> <span class="lnr lnr-calendar-full"></span></p>
								<p class="view col-lg-12 col-md-12 col-6"><a href="#">1.2M Views</a> <span class="lnr lnr-eye"></span></p>
								<p class="comments col-lg-12 col-md-12 col-6"><a href="#">06 Comments</a> <span class="lnr lnr-bubble"></span></p>						
							</div>
						</div>
						<div class="col-lg-9 col-md-9">
						
							<a class="posts-title" href="notice-detail"><h3>The Glossary Of Telescopes</h3></a>
							<p class="excert">
								MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
							</p>
							<a href="notice-detail" class="primary-btn">View More</a>
						</div>
					</div>
					<div class="single-post row">
						<div class="col-lg-3  col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6"><a href="#">Mark wiens</a> <span class="lnr lnr-user"></span></p>
								<p class="date col-lg-12 col-md-12 col-6"><a href="#">12 Dec, 2017</a> <span class="lnr lnr-calendar-full"></span></p>
								<p class="view col-lg-12 col-md-12 col-6"><a href="#">1.2M Views</a> <span class="lnr lnr-eye"></span></p>
								<p class="comments col-lg-12 col-md-12 col-6"><a href="#">06 Comments</a> <span class="lnr lnr-bubble"></span></p>						
							</div>
						</div>
						<div class="col-lg-9 col-md-9">
							
							<a class="posts-title" href="notice-detail"><h3>The Night Sky</h3></a>
							<p class="excert">
								MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
							</p>
							<a href="notice-detail" class="primary-btn">View More</a>
						</div>
					</div>
					<div class="single-post row">
						<div class="col-lg-3 col-md-3 meta-details">
							<div class="user-details row">
								<p class="user-name col-lg-12 col-md-12 col-6"><a href="#">Mark wiens</a> <span class="lnr lnr-user"></span></p>
								<p class="date col-lg-12 col-md-12 col-6"><a href="#">12 Dec, 2017</a> <span class="lnr lnr-calendar-full"></span></p>
								<p class="view col-lg-12 col-md-12 col-6"><a href="#">1.2M Views</a> <span class="lnr lnr-eye"></span></p>
								<p class="comments col-lg-12 col-md-12 col-6"><a href="#">06 Comments</a> <span class="lnr lnr-bubble"></span></p>						
							</div>
						</div>
						<div class="col-lg-9 col-md-9">
							
							<a class="posts-title" href="notice-detail"><h3>Telescopes 101</h3></a>
							<p class="excert">
								MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
							</p>
							<a href="notice-detail" class="primary-btn">View More</a>
						</div>
					</div>														
                    <nav class="blog-pagination justify-content-center d-flex">
                        <ul class="pagination">
                            <li class="page-item">
                                <a href="#" class="page-link" aria-label="Previous">
                                    <span aria-hidden="true">
                                        <span class="lnr lnr-chevron-left"></span>
                                    </span>
                                </a>
                            </li>
                            <li class="page-item active"><a href="#" class="page-link">01</a></li>
                            <li class="page-item"><a href="#" class="page-link">02</a></li>
                            <li class="page-item"><a href="#" class="page-link">03</a></li>
                            <li class="page-item"><a href="#" class="page-link">04</a></li>
                            <li class="page-item"><a href="#" class="page-link">09</a></li>
                            <li class="page-item">
                                <a href="#" class="page-link" aria-label="Next">
                                    <span aria-hidden="true">
                                        <span class="lnr lnr-chevron-right"></span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </nav>
				</div>
				<div class="col-lg-4 sidebar-widgets">
					<div class="widget-wrap">
						<div class="single-sidebar-widget search-widget">
							<form class="search-form" action="#">
	                            <input placeholder="Search Posts" name="search" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'" >
	                            <button type="submit"><i class="fa fa-search"></i></button>
	                        </form>
						</div>
						<div class="single-sidebar-widget user-info-widget">
							<a href="notice-write" class="genric-btn primary e-large">게시글 쓰기</a>
						</div>
					</div>
<!-- 							<ul class="social-links"> -->
<!-- 								<li><a href="#"><i class="fa fa-facebook"></i></a></li> -->
<!-- 								<li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
<!-- 								<li><a href="#"><i class="fa fa-github"></i></a></li> -->
<!-- 								<li><a href="#"><i class="fa fa-behance"></i></a></li> -->
<!-- 							</ul> -->
<!-- 							<p> -->
<!-- 								Boot camps have its supporters andit sdetractors. Some people do not understand why you should have to spend money on boot camp when you can get. Boot camps have itssuppor ters andits detractors. -->
<!-- 							</p> -->
<!-- 						</div> -->
<!-- 						<div class="single-sidebar-widget popular-post-widget"> -->
<!-- 							<h4 class="popular-title">Popular Posts</h4> -->
<!-- 							<div class="popular-post-list"> -->
<!-- 								<div class="single-post-list d-flex flex-row align-items-center"> -->
<!-- 									<div class="thumb"> -->
<!-- 										<img class="img-fluid" src="img/blog/pp1.jpg" alt=""> -->
<!-- 									</div> -->
<!-- 									<div class="details"> -->
<!-- 										<a href="blog-single.html"><h6>Space The Final Frontier</h6></a> -->
<!-- 										<p>02 Hours ago</p> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="single-post-list d-flex flex-row align-items-center"> -->
<!-- 									<div class="thumb"> -->
<!-- 										<img class="img-fluid" src="resources/img/blog/pp2.jpg" alt=""> -->
<!-- 									</div> -->
<!-- 									<div class="details"> -->
<!-- 										<a href="blog-single.html"><h6>The Amazing Hubble</h6></a> -->
<!-- 										<p>02 Hours ago</p> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="single-post-list d-flex flex-row align-items-center"> -->
<!-- 									<div class="thumb"> -->
<!-- 										<img class="img-fluid" src="resources/img/blog/pp3.jpg" alt=""> -->
<!-- 									</div> -->
<!-- 									<div class="details"> -->
<!-- 										<a href="blog-single.html"><h6>Astronomy Or Astrology</h6></a> -->
<!-- 										<p>02 Hours ago</p> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="single-post-list d-flex flex-row align-items-center"> -->
<!-- 									<div class="thumb"> -->
<!-- 										<img class="img-fluid" src="resources/img/blog/pp4.jpg" alt=""> -->
<!-- 									</div> -->
<!-- 									<div class="details"> -->
<!-- 										<a href="blog-single.html"><h6>Asteroids telescope</h6></a> -->
<!-- 										<p>02 Hours ago</p> -->
<!-- 									</div> -->
<!-- 								</div>															 -->
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 						<div class="single-sidebar-widget ads-widget"> -->
<!-- 							<a href="#"><img class="img-fluid" src="resources/img/blog/ads-banner.jpg" alt=""></a> -->
<!-- 						</div> -->
<!-- 						<div class="single-sidebar-widget post-category-widget"> -->
<!-- 							<h4 class="category-title">Post Catgories</h4> -->
<!-- 							<ul class="cat-list"> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Technology</p> -->
<!-- 										<p>37</p> -->
<!-- 									</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Lifestyle</p> -->
<!-- 										<p>24</p> -->
<!-- 									</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Fashion</p> -->
<!-- 										<p>59</p> -->
<!-- 									</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Art</p> -->
<!-- 										<p>29</p> -->
<!-- 									</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Food</p> -->
<!-- 										<p>15</p> -->
<!-- 									</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Architecture</p> -->
<!-- 										<p>09</p> -->
<!-- 									</a> -->
<!-- 								</li> -->
<!-- 								<li> -->
<!-- 									<a href="#" class="d-flex justify-content-between"> -->
<!-- 										<p>Adventure</p> -->
<!-- 										<p>44</p> -->
<!-- 									</a> -->
<!-- 								</li>															 -->
<!-- 							</ul> -->
<!-- 						</div>	 -->
<!-- 						<div class="single-sidebar-widget newsletter-widget"> -->
<!-- 							<h4 class="newsletter-title">Newsletter</h4> -->
<!-- 							<p> -->
<!-- 								Here, I focus on a range of items and features that we use in life without -->
<!-- 								giving them a second thought. -->
<!-- 							</p> -->
<!-- 							<div class="form-group d-flex flex-row"> -->
<!-- 							   <div class="col-autos"> -->
<!-- 							      <div class="input-group"> -->
<!-- 							        <div class="input-group-prepend"> -->
<!-- 							          <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i> -->
<!-- 									</div> -->
<!-- 							        </div> -->
<!-- 							        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" > -->
<!-- 							      </div> -->
<!-- 							    </div> -->
<!-- 							    <a href="#" class="bbtns">Subcribe</a> -->
<!-- 							</div>	 -->
<!-- 							<p class="text-bottom"> -->
<!-- 								You can unsubscribe at any time -->
<!-- 							</p>								 -->
<!-- 						</div> -->
<!-- 						<div class="single-sidebar-widget tag-cloud-widget"> -->
<!-- 							<h4 class="tagcloud-title">Tag Clouds</h4> -->
<!-- 							<ul> -->
<!-- 								<li><a href="#">Technology</a></li> -->
<!-- 								<li><a href="#">Fashion</a></li> -->
<!-- 								<li><a href="#">Architecture</a></li> -->
<!-- 								<li><a href="#">Fashion</a></li> -->
<!-- 								<li><a href="#">Food</a></li> -->
<!-- 								<li><a href="#">Technology</a></li> -->
<!-- 								<li><a href="#">Lifestyle</a></li> -->
<!-- 								<li><a href="#">Art</a></li> -->
<!-- 								<li><a href="#">Adventure</a></li> -->
<!-- 								<li><a href="#">Food</a></li> -->
<!-- 								<li><a href="#">Lifestyle</a></li> -->
<!-- 								<li><a href="#">Adventure</a></li> -->
<!-- 							</ul> -->
<!-- 						</div>								 -->
<!-- 					</div> -->
				</div>
			</div>
		</div>	
	</section>
	<!-- End post-content Area -->
			
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>