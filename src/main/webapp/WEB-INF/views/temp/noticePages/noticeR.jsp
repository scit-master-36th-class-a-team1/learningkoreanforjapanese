<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>공지사항 글쓰기</title>
	
	<style>
		div#wrapper{
			width : 800px;
			margin : 0 auto;
		}
		div#wrapper>h2 {
			text-align : center;
		}
		th {
			width: 100px;
		}
		td {
			width:600px;
		}
		div#wrapper table {
			margin:0 auto;
		}
	</style>
	
	<!-- Make sure the path to CKEditor is correct. -->
	<script src="ckeditor/ckeditor.js"></script>
</head>
<body>
	<div id="wrapper">
		<h2>[ 공지사항 글쓰기 ]</h2>
		
		<form action="noticeRegist" method="post">
			<table border="1">
				<tr>
					<th>제목</th>
					<td><input type="text" name="notice_name" required/></td>
				</tr>
				<tr>
					<th>글쓴이</th>
			<%-- 		<td>${sessionScope.loginName }</td> --%>
					<td><input type="text" name="administrator_id"/></td>
				</tr>
				<tr>
					<th>글내용</th>
					<td>
						<textarea id="notice_content" name="notice_content" cols="60" rows="10"></textarea>
						
						<script>
							CKEDITOR.on('dialogDefinition', function (ev) {
						        var dialogName = ev.data.name;
						        var dialog = ev.data.definition.dialog;
						        var dialogDefinition = ev.data.definition;
						        
						        if (dialogName == 'image') {
									dialog.on('show', function (obj) {
										this.selectPage('Upload'); 
									});
									
									dialogDefinition.removeContents('advanced');
									dialogDefinition.removeContents('Link');
								}
							});
							
							var ckeditor_config = {
								language : 'ko' // 언어 설정
								, resize_enabled : false // 크기 조정막기
// 								, uiColor : '#cccccc' // 에디터 색상
								, enterMode : CKEDITOR.ENTER_BR
								, shiftEnterMode : CKEDITOR.ENTER_P
								, filebrowserUploadMethod : "form"
								, filebrowserUploadUrl : "ckEditorImgUpload" // 업로드 액션 정의
							};
				
							// 첫 번째 변수: textarea의 id
							CKEDITOR.replace('notice_content', ckeditor_config);
							
							// 전송을 위한 체크 함수
							function form_save(form) {
								editor.updateElement();
							}
						</script>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="글등록" />
						<input type="reset" value="취소" />
						<a href="noticeList" >목록으로</a>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>