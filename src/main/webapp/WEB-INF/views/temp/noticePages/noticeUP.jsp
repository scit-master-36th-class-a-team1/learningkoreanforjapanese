<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>        
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>공지사항 글 수정</title>
	
	<style>
		div#wrapper{
			width : 800px;
			margin : 0 auto;
		}
		div#wrapper>h2 {
			text-align : center;
		}
		div#wrapper table {
			margin:0 auto;
			width : 800px;
		}
		th {
			width: 100px;
		}
		pre {
			width : 600px;
			height : 200px;
			overflow: auto;
		}
		table.reply {
			width : 800px;
		}
		input[name='text'] {
			width : 600px;
		}
		span {
			display: inline-block;
			margin : 5px;
		}
		td.replycontent {
			width : auto;
		}
		td.replytext {
			width : 600px;
			text-align : left;
		}
		td.replysub {
			width : 80px;
			text-align : right;
		}
		td.replybtn{
			width : 100px;
			text-align : right;
		}
	</style>
	
	<!-- Make sure the path to CKEditor is correct. -->
	<script src="ckeditor/ckeditor.js"></script>
</head>
<body>
	<div id="wrapper">
		<h2>[ 공지사항 글 수정 ]</h2>
		
		<form action="noticeUpdate" method="post">
			<input type="hidden" name="notice_num" value="${notice.notice_num }">
			<table border="1">
				<tr>
					<th>제목</th>
					<td><input type="text" name="notice_name" value="${notice.notice_name }"></td>
				</tr>
				<tr>
					<th>글 쓴날</th>
					<td>${notice.notice_date }</td>
				</tr>	
				<tr>
					<th>글쓴이</th>
	<%-- 				<td>${sessionScope.administor_id }</td> --%>
					<td>${notice.administrator_id }</td>
				</tr>
				<tr>
					<th>글내용</th>
					<td>
						<textarea name="notice_content" rows="10" cols="60">
							${notice.notice_content }
						</textarea>
						
						<script>
							CKEDITOR.on('dialogDefinition', function (ev) {
						        var dialogName = ev.data.name;
						        var dialog = ev.data.definition.dialog;
						        var dialogDefinition = ev.data.definition;
						        
						        if (dialogName == 'image') {
									dialog.on('show', function (obj) {
										this.selectPage('Upload'); 
									});
									
									dialogDefinition.removeContents('advanced');
									dialogDefinition.removeContents('Link');
								}
							});
							
							var ckeditor_config = {
								language : 'ko' // 언어 설정
								, resize_enabled : false // 크기 조정막기
// 								, uiColor : '#cccccc' // 에디터 색상
								, enterMode : CKEDITOR.ENTER_BR
								, shiftEnterMode : CKEDITOR.ENTER_P
								, filebrowserUploadMethod : "form"
								, filebrowserUploadUrl : "ckEditorImgUpload" // 업로드 액션 정의
							};
				
							// 첫 번째 변수: textarea의 id
							CKEDITOR.replace('notice_content', ckeditor_config);
							
							// 전송을 위한 체크 함수
							function form_save(form) {
								editor.updateElement();
							}
						</script>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="글수정">
						<input type="reset" value="취소">
						<a href="noticeList">목록으로</a>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>