<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
	<head>
	<!-- Set Title -->
	<title>Notice | K.du</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
</head>
<body>
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative blog-home-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content blog-header-content col-lg-12">
					<h1 class="text-white">
						Notice		
					</h1>	
					<p class="text-white">
						There is a moment in the life of any aspiring astronomer that it is time to buy that first
					</p>
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area				   -->
			
			<!-- 특정 글 검색 -->
			<form id="search" action ="noticeList" method="get" >
				<select name="searchItem"> 
					<option value="notice_name" 		${searchItem == 'notice_name' 		? 'selected' : '' }>제목</option>
					<option value="administrator_id" 	${searchItem == 'administrator_id' 	? 'selected' : '' }>작성자</option>
					<option value="notice_content" 		${searchItem == 'notice_content' 	? 'selected' : '' }>내용</option>
				</select>
				
				<input type="text" name="searchWord" value="${searchWord }" /> 
				<input class="btn" type="submit" value="검색" />
			</form>
		</div>
		
		<!-- 게시글 목록 시작 -->
		<table border='1'>
			<tr>
				<th>번호</th>
				<th class="title">제목</th>
				<th>날짜</th>
				<th>글쓴이</th>
				<th>조회수</th>
			</tr>
			
			<!-- 게시글 출력 반복 -->
			<c:forEach var="notice" items="${noticeList }" varStatus="stat">
				<tr>
					<td>${stat.count + navi.startRecord }</td>
					<td><a href="noticeDetail?notice_num=${notice.notice_num }">${notice.notice_name }</a></td>
					<td>${notice.notice_date }</td>
					<td>${notice.administrator_id }</td>
					<td>${notice.notice_hitcount }</td>
				</tr>
			</c:forEach>
		</table>
		
		<!-- 운영자로 로그인 되어있을 때만 글쓰기 가능 -->
		<%-- <c:if test="${sessionScope.administorId != null }"> --%>
			<div class="write"><a class="btn" href="noticeRegist">글쓰기</a></div>
		<%-- </c:if> --%>
		
		<!-- Paging 출력 부분 -->
		<div id="navigator">
			<a href="noticeList?currentPage=${navi.currentPage - navi.pagePerGroup }&searchItem=${searchItem }&searchWord=${searchWord }">
				◁◁
			</a>
			<a href="noticeList?currentPage=${navi.currentPage - 1}&searchItem=${searchItem }&searchWord=${searchWord }">
				◀
			</a>
			&nbsp;&nbsp;
			<c:forEach var="page" begin="${navi.startPageGroup }" end="${navi.endPageGroup }">
				<c:if test="${page != navi.currentPage}">
					<a href="noticeList?currentPage=${page }&searchItem=${searchItem }&searchWord=${searchWord }">${page }</a> &nbsp;
				</c:if>
				<c:if test="${page == navi.currentPage}">
					<span style="color: red; font-size: 1.5em;">${page }</span> &nbsp;
				</c:if>
			</c:forEach>
			&nbsp;&nbsp;
			<a href="noticeList?currentPage=${navi.currentPage + 1 }&searchItem=${searchItem }&searchWord=${searchWord }">
				▶ 
			</a> 
			<a href="noticeList?currentPage=${navi.currentPage + navi.pagePerGroup}&searchItem=${searchItem }&searchWord=${searchWord }">
				▷▷
			</a>
		</div>
	</div>
</body>
</html>