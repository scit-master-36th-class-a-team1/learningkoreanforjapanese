<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>        
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>공지사항 글보기</title>
	
	<style>
		div#wrapper{
			width: 800px;
			margin: 0 auto;
		}
		div#wrapper>h2 {
			text-align: center;
		}
		div#wrapper table {
			margin: 0 auto;
			width : 800px;
		}
		th {
			width: 100px;
		}
		pre {
			width : 600px;
			height : 200px;
			overflow: auto;
		}
		table.reply {
			width : 800px;
		}
		input[name='text'] {
			width : 600px;
		}
		span {
			display: inline-block;
	
			margin : 5px;
		}
		td.replycontent {
			width : auto;
		}
		td.replytext {
			width : 600px;
			text-align : left;
		}
		td.replysub {
			width : 80px;
			text-align : right;
		}
		td.replybtn{
			width : 100px;
			text-align : right;
		}
	</style>
</head>
<body>
	<div id="wrapper">
		<h2>[ 공지사항 글보기 ]</h2>
		<table border="1">
			<tr>
				<th>제목</th>
				<td>${notice.notice_name }</td>
			</tr>
			<tr>
				<th>글쓴날</th>
				<td>${notice.notice_date }</td>
			</tr>	
			<tr>
				<th>글쓴이</th>
				<td>${notice.administrator_id }</td>
			</tr>
			<tr>
				<th>글내용</th>
				<td>
					<pre>${notice.notice_content }</pre>
				</td>
			</tr>
			<tr>
				<td colspan="2">
		<%-- 			<c:if test="${sessionScope.administorId == notice.administor_id }"> --%>
						<input type="button" value="글수정" onclick="location.href='noticeUpdate?notice_num=${notice.notice_num }'"/>
						<input type="button" value="글삭제" onclick="location.href='noticeDelete?notice_num=${notice.notice_num }'"/>
		<%-- 			</c:if> --%>
					<a href="noticeList">목록으로</a>
				</td>
			</tr>
		</table>
		
		<br />
	</div>
</body>
</html>