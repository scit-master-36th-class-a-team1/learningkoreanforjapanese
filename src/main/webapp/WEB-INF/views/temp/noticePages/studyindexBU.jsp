<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<!-- Set Title -->
	<title>Study Index</title>
	
	<!-- Google Signin Client Id -->
	<meta name="google-signin-client_id" content="915967161999-3cdg6v06b7dm1agkqg3n55dmoq9a7tte.apps.googleusercontent.com">
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="resources/img/logo-favicon.ico" />
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
		
	<!-- CSS -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="resources/css/linearicons.css">
	<link rel="stylesheet" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<link rel="stylesheet" href="resources/css/magnific-popup.css">
	<link rel="stylesheet" href="resources/css/nice-select.css">
	<link rel="stylesheet" href="resources/css/animate.min.css">
	<link rel="stylesheet" href="resources/css/owl.carousel.css">
	<link rel="stylesheet" href="resources/css/jquery-ui.css">
	<link rel="stylesheet" href="resources/css/main.css">
	
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script type="text/javascript">
	
	</script>
</head>
<body>
	
	<!-- Start header -->
	<header id="header"></header>
	<!-- End header -->
	
	<!-- start banner Area -->
			<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Level 1				
							</h1>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->			
	
	<!-- Start top-category-widget Area -->
	<section class="top-category-widget-area pt-90 pb-90 ">
	</section>
	<!-- End top-category-widget Area -->
			
	<!-- Start post-content Area -->
	<section class="post-content-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 posts-list">
							<h3 class="mb-30">글 검색</h3>
							<div class="default-select" id="default-select">
								<form id="search" action ="studyIndex" method="get" >
									<select name="searchItem">
										<option value="study_mainTitle" ${searchItem=='study_mainTitle' ? 'selected' : '' }>제목</option>
										<option value="study_subTitle" ${searchItem=='study_subTitle' ? 'selected' : '' }>부제목</option>
									</select>
									<br>
									<input type="text" name="study_type" readonly="readonly" value="${study_type}" >
									<br>
									<input type="text" name="searchWord" value="${searchWord }" /> 
									<input class="btn" type="submit" value="검색" />
								</form>
							</div>
						</div>
					</div>
				</div>
	</section>
	<!-- End Searching study Area -->
	<!-- Start Button -->
	<section class="button-area">
		<div class="container border-top-generic">
			<div class="button-group-area">
				<a href="#" class="genric-btn info">확인</a>
	<!-- Start Write 관리자 전용 테스트를 위해 주석처리 해두었으니 나중에 주석 제거 -->
				<c:if test="${sessionScope.administrator_id != null}">
					<a href="studyRegist" class="genric-btn primary">글쓰기</a>
				</c:if>
	<!-- End Write -->
			</div>
		</div>
	</section>
	<!-- End Button -->
	
	<!-- Start Main Area -->
	<c:forEach var="study" items="${studyList }" >
		<section class="sample-text-area">
			<div class="container">
				<h3 class="text-heading">
						${study.study_mainTitle}
				</h3>
				<p class="sample-text">
					<a href="studyDetail?currentPage=${navi.currentPage}&searchItem=${searchItem}&searchWord=${searchWord}&study_num=${study.study_num}&study_type=${study_type}">
					${study.study_subTitle }
					</a>
					<br>
					<input type="button" value="글삭제" onclick="location.href='studyDelete?study_num=${study.study_num}'" />
				</p>
			</div>
		</section>
	</c:forEach>
	<!-- End Main Area -->
	
	<!-- Start Paging Area -->
	<div id="navigator">
	<input type="text" id = "study_type" name="study_type" hidden="hidden" value="${study_type}" >
		<a href="studyIndex?currentPage=${navi.currentPage-navi.pagePerGroup}&searchItem=${searchItem }&searchWord=${searchWord}">
			◁◁
		</a>
		<a href="studyIndex?currentPage=${navi.currentPage-1}&searchItem=${searchItem }&searchWord=${searchWord}">
			◀
		</a>
		
		&nbsp;&nbsp;
		<c:forEach var="page" begin="${navi.startPageGroup }" end="${navi.endPageGroup }">
			<c:if test="${navi.currentPage==page }">
				<span style="color:red; font-size: 2em;">${page }</span>&nbsp;
			</c:if>
			<c:if test="${navi.currentPage!=page }">
				<a href="studyIndex?currentPage=${page }&searchItem=${searchItem }&searchWord=${searchWord}">${page } </a>&nbsp;
			</c:if>
		</c:forEach>
	&nbsp;&nbsp;
	
		<a href="studyIndex?currentPage=${navi.currentPage+1}&searchItem=${searchItem }&searchWord=${searchWord}">
			▶
		 </a> 
		<a href="studyIndex?currentPage=${navi.currentPage+navi.pagePerGroup}&searchItem=${searchItem }&searchWord=${searchWord}">
			▷▷
		</a>
	</div>
	<!-- End Paging Area -->
	
	<!-- start footer Area -->
	<footer class="footer-area section-gap"></footer>
	<!-- End footer Area -->
	
	<!-- Script -->
	<script src="resources/js/vendor/jquery-3.3.1.min.js"></script>
	<script>
		$("header").load('defaultStructure header > *');
		$("footer").load('defaultStructure footer > *');
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="resources/js/googleLogin.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="resources/js/vendor/bootstrap.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="resources/js/easing.min.js"></script>
	<script src="resources/js/hoverIntent.js"></script>
	<script src="resources/js/superfish.min.js"></script>
	<script src="resources/js/jquery.ajaxchimp.min.js"></script>
	<script src="resources/js/jquery.magnific-popup.min.js"></script>
  	<script src="resources/js/jquery.tabs.min.js"></script>
	<script src="resources/js/jquery.nice-select.min.js"></script>
	<script src="resources/js/owl.carousel.min.js"></script>
	<script src="resources/js/mail-script.js"></script>
	<script src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/main.js"></script>
</body>
</html>