<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="js/vendor/jquery-3.3.1.min.js"></script>
<script>
	$(function() {
		$("#start").on("click", startSTT);
		
		$("#delete").on("click", function(){
			$("#result").val('');
		})
	});
	
	function startSTT() {
		$.get({
			url : "startSTT"
			, contentType: "application/x-www-form-urlencoded; charset=UTF-8"
			, dataType:'text'
			, success : function(resp) {
				alert(resp);
				$("#result").val(resp);
			}
			, error : function(){
				alert("실패");
			}
		});
	}
</script>
</head>
<body>
	<h1>임시 첫 페이지</h1>
	<input type="button" id="start" value="녹음 시작" />
	<br>
	<input type="text" id="result" readonly="readonly" />
	<br>
	<input type="button" id="delete" value="삭제" />
	<br>
	<a href="index">index</a>
</body>
</html>