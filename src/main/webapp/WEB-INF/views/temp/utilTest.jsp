<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="js/vendor/jquery-3.3.1.min.js"></script>
<script>
	$(function() {
		$("#start").on("click", startSTT);
		$("#deleteSTT").on("click", function(){
			$("#result").val('');
		});
		$("#testbtn").on("click", test);
	});

	function startSTT() {
		$.get({
			url : "startSTT",
			contentType : "application/x-www-form-urlencoded; charset=UTF-8",
			dataType : 'text',
			success : function(resp) {
				alert(resp);
				$("#result").val(resp);
			},
			error : function() {
				alert("실패");
			}
		});
	}
	
	function test(){
		$.get({
			url : "https://texttospeech.googleapis.com/v1beta1/text:synthesize",
			data : {
				  		"audioConfig": {
					    "audioEncoding": "LINEAR16",
					    "pitch": 0,
					    "speakingRate": 1
					  },
					  "input": {
					    "text": "구글"
					  },
					  "voice": {
					    "languageCode": "ko-KR",
					    "name": "ko-KR-Wavenet-B"
					  }
					},
			success : function(){
				alert("success");
			},
			error : function(){
				alert("error");
			}
		});
	}
	
	
</script>
</head>
<body>
	<h1>utilTest</h1>
	<input type="button" id="start" value="녹음 시작" />
	<br>
	<input type="text" id="result" readonly="readonly" />
	<br>
	<button id="deleteSTT"> 내용 지우기  </button>
	<br>
	
	<br>
<!-- 	<input type="text" id="test"> -->
<!-- 	<button id="testbtn"> 테스트  </button> -->
	-----------------------------------------------
	
	<form action="speak" method="get">
		Text To Speech<input type="text" name="text" />
		<button>전송</button>
	</form>
	TSS
	
	<br>
	<audio id="myAudio" controls>
		<source src="resources/mp3/3.mp3" type="audio/mpeg">
	</audio>
</body>
</body>
</html>