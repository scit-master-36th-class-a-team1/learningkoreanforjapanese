/**
 * textarea의 id를 받아 해당 영역에 CKEditor를 연결하는 script
 */

function addCustomCKEditor(formId, textareaId) {
	CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialog = ev.data.definition.dialog;
        var dialogDefinition = ev.data.definition;
        
        if (dialogName == 'image') {
			dialog.on('show', function (obj) {
				this.selectPage('Upload'); 
			});
			
			dialogDefinition.removeContents('advanced');
			dialogDefinition.removeContents('Link');
		}
	});
	
	var ckeditor_config = {
		language : 'ko' // 언어 설정
		, resize_enabled : false // 크기 조정막기
//			, uiColor : '#cccccc' // 에디터 색상
		, enterMode : CKEDITOR.ENTER_BR
		, shiftEnterMode : CKEDITOR.ENTER_P
		, filebrowserUploadMethod : "form"
		, filebrowserUploadUrl : "ckEditorImgUpload" // 업로드 액션 정의
		, height:'350px'
	};

	// 첫 번째 변수: textarea의 id
	CKEDITOR.replace(textareaId, ckeditor_config);
	
	// submit check
	var inputFormId = "#" + formId;
	$(inputFormId).submit(function(event) {
        var messageLength = CKEDITOR.instances[textareaId].getData().replace(/<[^>]*>/gi, '').length;
        
        if(!messageLength) {
            alert('글 내용을 입력해주세요.');
            event.preventDefault();
        }
    });
}