function onSignIn(googleUser) {
	//로그인 된 상태면 언제나 이 펑션을 불러온다. 

	var profile = googleUser.getBasicProfile();
//	콘솔로 받은 정보 확인
//	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
//	console.log('Name: ' + profile.getName());
//	console.log('Image URL: ' + profile.getImageUrl());
//	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	
//	구글 로그인 후 이메일 정보 받아오기
	
	member_email = profile.getEmail();
//	$.ajax({
//		type : 'get'
//		, data : {member_email : member_email}
//		, url : 'nicknameCheckforAjax'
//		, success : function(nickname){
//			console.log('ajax 닉네임 체크' + nickname);
//			return;
//		}
//	})
	
	$.ajax({
		type : 'get'
		, data : {member_email : member_email}
		, url : 'emailCheck'
		, success : function(m){
			if(m.member_email == null) {
				console.log('email 정보 없음')
				location.href = 'nickname?member_email='+member_email;
				return;
			}
			if(m.member_email != null){
				$.ajax({
					type : 'get'
					, data : {member_email : m.member_email}
					, url : 'loginSession'
					, success : function(){
						location.href='index';
					}
				})
//				alert('goolgeLogin.js : ' + m.member_email);
//				location.href='loginSession?member='+m.member_email;
				return;
			} 
		}
	});
}

// 로그인 실패 시 
function onFailure(error) {
	console.log(error);
}

// 로그아웃
function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function() {
		console.log('User signed out.');
		location.href='googleLogout';
	});
}

// 계정 삭제
function deleteAccount() {
	var result = confirm('本当にアカウントを消しますか?\n削除されたアカウントは復旧できません。');
	
	// 취소 버튼을 누르면 삭제하지 않음
	if (!result) {
		return;
	}
	
	// 확인 버튼을 누르면 계정 삭제를 진행함
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function() {
		console.log('User signed out.');
		location.href='deleteAccount';
	});
}
